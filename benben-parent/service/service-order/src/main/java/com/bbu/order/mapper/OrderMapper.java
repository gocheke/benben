package com.bbu.order.mapper;

import com.bbu.order.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2022-11-30
 */
public interface OrderMapper extends BaseMapper<Order> {

}
