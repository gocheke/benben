package com.bbu.order.service;

import com.bbu.order.entity.PayLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 支付日志表 服务类
 * </p>
 *
 * @author testjava
 * @since 2022-11-30
 */
public interface PayLogService extends IService<PayLog> {

    Map createNative(String orderNo);

    void updatePayStatus(Map<String,String> map);

    Map queryPaystatus(String orderNo);
}
