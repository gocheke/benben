package com.bbu.order.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bbu.common.utils.JwtUtils;
import com.bbu.common.utils.Result;
import com.bbu.order.entity.Order;
import com.bbu.order.service.OrderService;
import com.netflix.client.http.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2022-11-30
 */
@RestController
@RequestMapping("/eduorder/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("createOrder/{courseId}")
    public Result createOrder(@PathVariable String courseId, HttpServletRequest request){

        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        String orderNo = orderService.ctreateOrder(courseId,memberId);

        return Result.ok().data("orderId",orderNo);
    }

    @GetMapping("getOrderInfo/{id}")
    public Result getOrderInfo(@PathVariable String id){

        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("order_no",id);

        Order order = orderService.getOne(wrapper);

        return Result.ok().data("order",order);
    }

    @GetMapping("QueryBuyStatu/{courseId}/{memberId}")
    public boolean QueryBuyStatu(@PathVariable String courseId ,@PathVariable String memberId){

        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        wrapper.eq("member_id",memberId);
        wrapper.eq("status",1);

        int count = orderService.count(wrapper);

        if (count > 0){
            return true;
        }


        return false;
    }

}

