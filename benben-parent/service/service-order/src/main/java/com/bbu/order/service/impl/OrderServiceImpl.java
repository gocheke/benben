package com.bbu.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bbu.common.utils.Result;
import com.bbu.common.vo.CourseWebVoOrder;
import com.bbu.common.vo.UcenterMemberOrder;
import com.bbu.order.client.EduClient;
import com.bbu.order.client.UcenterClient;
import com.bbu.order.entity.Order;
import com.bbu.order.mapper.OrderMapper;
import com.bbu.order.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bbu.order.utils.OrderNoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2022-11-30
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private EduClient eduClient;

    @Autowired
    private UcenterClient ucenterClient;

    @Override
    public String ctreateOrder(String courseId, String memberId) {

        // 根据远程调用获取用户信息
        UcenterMemberOrder memberOrder = ucenterClient.getUserInfoOrder(memberId);

        // 根据远程调用获取课程信息
        CourseWebVoOrder courseInfo = eduClient.getCourseInfo(courseId);

        Order order = new Order();
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseId);
        order.setCourseTitle(courseInfo.getTitle());
        order.setCourseCover(courseInfo.getCover());
        order.setTeacherName(courseInfo.getTeacherName());
        order.setTotalFee(courseInfo.getPrice());
        order.setMemberId(memberId);
        order.setMobile(memberOrder.getMobile());
        order.setNickname(memberOrder.getNickname());
        order.setStatus(0);
        order.setPayType(1);
        baseMapper.insert(order);

        return order.getOrderNo();
    }



}
