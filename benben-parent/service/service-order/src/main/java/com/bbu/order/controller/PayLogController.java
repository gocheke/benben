package com.bbu.order.controller;


import com.bbu.common.utils.Result;
import com.bbu.order.service.PayLogService;
import com.bbu.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 支付日志表 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2022-11-30
 */
@RestController
@RequestMapping("/eduorder/paylog")
public class PayLogController {

    @Autowired
    private PayLogService payLogService;

    @PostMapping("createNative/{orderNo}")
    public Result createNative(@PathVariable String orderNo){

        Map map = payLogService.createNative(orderNo);
//        System.out.println("创建支付" + map);

        return Result.ok().data(map);
    }

    @GetMapping("queryPayStatus/{orderNo}")
    public Result queryPayStatus(@PathVariable String orderNo){

        Map<String,String> map = payLogService.queryPaystatus(orderNo);
//        System.out.println("查询支付状态" + map);

        if (map == null){
            return Result.error().message("支付出错了");
        }

        if (map.get("trade_state").equals("SUCCESS")){

            payLogService.updatePayStatus(map);

            return Result.ok();
        }

        return Result.ok().code(25000).message("正在支付中");

    }

}

