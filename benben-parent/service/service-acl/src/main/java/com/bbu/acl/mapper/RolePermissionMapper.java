package com.bbu.acl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bbu.acl.entity.RolePermission;

/**
 * <p>
 * 角色权限 Mapper 接口
 * </p>
 *
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
