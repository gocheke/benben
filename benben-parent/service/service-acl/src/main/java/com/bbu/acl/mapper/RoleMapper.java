package com.bbu.acl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bbu.acl.entity.Role;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface RoleMapper extends BaseMapper<Role> {

}
