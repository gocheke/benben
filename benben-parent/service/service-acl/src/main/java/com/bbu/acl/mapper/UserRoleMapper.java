package com.bbu.acl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bbu.acl.entity.UserRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
