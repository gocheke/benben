package com.bbu.oss.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class OssUploadFileUtil {
    /**
     * 专门负责上传文件到 OSS 服务器的工具方法
     *
     * @return 包含上传结果以及上传的文件在 OSS 上的访问路径
     */
    public static String uploadFileToOss(MultipartFile file) {

        String endpoint = ConstantPropertiesUtil.END_POINT;
        String accessKeyId = ConstantPropertiesUtil.ACCESS_KEY_ID;
        String accessKeySecret = ConstantPropertiesUtil.ACCESS_KEY_SECRET;
        String bucketName = ConstantPropertiesUtil.BUCKET_NAME;

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        try {
            InputStream inputStream = file.getInputStream();

            String originalName = file.getOriginalFilename();

            String suffix = originalName.substring(originalName.lastIndexOf("."));

            String uuid = UUID.randomUUID().toString().replace("-", "");

            String fileName = uuid + suffix;

            String folderName = new SimpleDateFormat("yyyy/MM/dd").format(new Date());

            String objectName = folderName + "/" + fileName;

            // 创建PutObject请求。
            ossClient.putObject(bucketName, objectName, inputStream);
            String url = "https://"+bucketName+"."+endpoint+"/"+objectName;
//            https://edu-chengke.oss-cn-nanjing.aliyuncs.com/default.jpg
            return url;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }

        return null;

    }
}
