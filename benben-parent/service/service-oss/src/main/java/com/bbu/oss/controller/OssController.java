package com.bbu.oss.controller;

import com.bbu.common.utils.Result;
import com.bbu.oss.service.OssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/eduoss/fileoss")
public class OssController {

    @Autowired
    OssService ossService;

    @PostMapping("file/upload")
    public Result uploadOssFile(MultipartFile file){
        // 返回上传到oss的路径
        String url = ossService.uploadFileAvatar(file);

        return Result.ok().data("url",url);
    }

}
