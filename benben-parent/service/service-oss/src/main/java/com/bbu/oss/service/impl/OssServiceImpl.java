package com.bbu.oss.service.impl;

import com.bbu.oss.service.OssService;
import com.bbu.oss.utils.OssUploadFileUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class OssServiceImpl implements OssService {
    @Override
    public String uploadFileAvatar(MultipartFile file) {

        String url = OssUploadFileUtil.uploadFileToOss(file);

        return url;
    }
}
