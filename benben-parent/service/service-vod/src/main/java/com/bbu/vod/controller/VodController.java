package com.bbu.vod.controller;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.DeleteVideoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.bbu.common.utils.Result;
import com.bbu.servicebase.exceptionhandler.GuliException;
import com.bbu.vod.service.VodService;
import com.bbu.vod.utils.ConstantPropertiesUtil;
import com.bbu.vod.utils.InitClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/eduvod/video")
public class VodController {

    @Autowired
    private VodService vodService;

    // 上传视频到阿里云
    @PostMapping("uploadVideo")
    public Result uploadVideo(MultipartFile file){

        String videoId = vodService.saveVideo(file);

        System.out.println(videoId);
        return Result.ok().data("VideoId",videoId);
    }

    @DeleteMapping("deleteVideo/{id}")
    public Result deleteVideo(@PathVariable("id") String id){

        try {
            DefaultAcsClient client = InitClient.initVodClient(ConstantPropertiesUtil.ACCESS_KEY_ID, ConstantPropertiesUtil.ACCESS_KEY_SECRET);

            DeleteVideoRequest request = new DeleteVideoRequest();

            request.setVideoIds(id);
            request.setSysEndpoint("vod.cn-shanghai.aliyuncs.com");

            DeleteVideoResponse response = client.getAcsResponse(request);
        } catch (ClientException e) {
            e.printStackTrace();
            throw new GuliException(20001,"删除视频失败");
        }

        return Result.ok();
    }

    @GetMapping("getPlayAuth/{id}")
    public Result getPlayAuth(@PathVariable String id){

        try {
            DefaultAcsClient client = InitClient.initVodClient(ConstantPropertiesUtil.ACCESS_KEY_ID, ConstantPropertiesUtil.ACCESS_KEY_SECRET);

            GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
            request.setVideoId(id);
            GetVideoPlayAuthResponse response = client.getAcsResponse(request);
            String playAuth = response.getPlayAuth();
            return Result.ok().data("playAuth",playAuth);
        } catch (ClientException e) {
            throw new GuliException(20001,"获取视频播放凭证失败");
        }
    }

}
