package com.bbu.vod.service;

import org.springframework.web.multipart.MultipartFile;

public interface VodService {
    String saveVideo(MultipartFile file);
}
