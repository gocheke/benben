package com.bbu.eduservice.mapper;

import com.bbu.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2022-11-29
 */
public interface EduCommentMapper extends BaseMapper<EduComment> {

}
