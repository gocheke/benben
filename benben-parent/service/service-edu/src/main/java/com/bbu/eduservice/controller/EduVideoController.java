package com.bbu.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bbu.common.utils.Result;
import com.bbu.eduservice.entity.EduVideo;
import com.bbu.eduservice.service.EduVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
@RestController
@RequestMapping("/eduservice/video")
public class EduVideoController {

    @Autowired
    private EduVideoService videoService;

    @GetMapping("getVideoInfo/{videoId}")
    public Result getVideoInfo(@PathVariable("videoId") String videoId){

        EduVideo eduVideo = videoService.getById(videoId);

        return Result.ok().data("eduvideo",eduVideo);
    }

    // 添加小节
    @PostMapping("addVideo")
    public Result addVideo(@RequestBody EduVideo eduVideo){

        videoService.save(eduVideo);

        return Result.ok();
    }

    // 更新小节
    @PostMapping("updateVideo/{id}")
    public Result updateVideo(@RequestBody EduVideo eduVideo,@PathVariable("id") String id){

        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();

        wrapper.eq("id",id);

        videoService.update(eduVideo,wrapper);

        return Result.ok();
    }

    // 删除小节
    // TODO 在删除小节时，需同时删除视频
    @DeleteMapping("deleteVideo/{videoId}")
    public Result deleteVideo(@PathVariable("videoId") String videoId){

        boolean remove = videoService.removeByVideoId(videoId);

        if (remove){
            return Result.ok();
        }else {
            return Result.error().message("删除失败");
        }
    }

}

