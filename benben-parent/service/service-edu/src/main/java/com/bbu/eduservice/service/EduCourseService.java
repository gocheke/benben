package com.bbu.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bbu.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bbu.eduservice.entity.chapter.PublishVo;
import com.bbu.eduservice.entity.frontvo.CourseFrontVo;
import com.bbu.eduservice.entity.frontvo.CourseWebVo;
import com.bbu.eduservice.entity.vo.CourseInfoVo;
import com.bbu.eduservice.entity.vo.CourseListVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
public interface EduCourseService extends IService<EduCourse> {

    String saveCourseInfo(CourseInfoVo courseInfoVo);

    CourseInfoVo getCourseInfoById(String courseId);

    void updateCourseInfoById(CourseInfoVo courseInfoVo);

    PublishVo getPublishInfo(String courseId);

    List<CourseListVo> getAllCourse();

    List<CourseListVo> getCoursePage(String title,String status,String subjectParentId,
                                     String subjectId,String teacherId,String begin,String end,
                                     Long current,Long limit);

    void removeCourse(String courseId);

    Map<String, Object> getCourseFrontList(Page<EduCourse> coursePage, CourseFrontVo courseFrontVo);

    CourseWebVo getCourseWebInfo(String id);
}
