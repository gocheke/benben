package com.bbu.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bbu.eduservice.entity.EduChapter;
import com.bbu.eduservice.entity.EduCourse;
import com.bbu.eduservice.entity.EduVideo;
import com.bbu.eduservice.entity.chapter.ChapterVo;
import com.bbu.eduservice.entity.chapter.VideoVo;
import com.bbu.eduservice.entity.vo.CourseInfoVo;
import com.bbu.eduservice.mapper.EduChapterMapper;
import com.bbu.eduservice.service.EduChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bbu.eduservice.service.EduVideoService;
import com.bbu.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    private EduVideoService eduVideoService;

    @Override
    public List<ChapterVo> getChapterVideoById(String courseId) {

        QueryWrapper<EduChapter> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id",courseId);

        List<EduChapter> chapterList = baseMapper.selectList(queryWrapper);

        ArrayList<ChapterVo> chapterVoList = new ArrayList<>();



        // 查出chapter并封装入chapterVo中
        for (int i = 0; i < chapterList.size(); i++) {

            EduChapter eduChapter = chapterList.get(i);
            ChapterVo chapterVo = new ChapterVo();
            BeanUtils.copyProperties(eduChapter,chapterVo);

            chapterVoList.add(chapterVo);


            String chapterId = chapterVo.getId();

            // 根据chapter的id查出video
            List<EduVideo> eduVideoList = eduVideoService.selectVideoListByChapterId(chapterId);

            if (eduVideoList != null){

                ArrayList<VideoVo> videoVoList = new ArrayList<>();

                for (int j = 0; j < eduVideoList.size(); j++) {
                    VideoVo videoVo = new VideoVo();
                    EduVideo eduVideo = eduVideoList.get(j);
                    BeanUtils.copyProperties(eduVideo,videoVo);
                    videoVoList.add(videoVo);
                }

                // 将videoVoList封装入chapterVO中
                chapterVo.setChildren(videoVoList);

            }

        }

        return chapterVoList;
    }

    @Override
    public boolean deleteById(String chapterId) {

        QueryWrapper<EduVideo> videoQueryWrapper = new QueryWrapper<>();
        videoQueryWrapper.eq("chapter_id",chapterId);

        int count = eduVideoService.count(videoQueryWrapper);

        if (count <= 0){

            int deleteId = baseMapper.deleteById(chapterId);

            if (deleteId <= 0){
                throw new GuliException(20001,"章节删除失败");
            }

        }else {
            return false;
        }

        return true;

    }

}
