package com.bbu.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bbu.common.utils.Result;
import com.bbu.eduservice.client.VodClient;
import com.bbu.eduservice.entity.EduVideo;
import com.bbu.eduservice.mapper.EduVideoMapper;
import com.bbu.eduservice.service.EduVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bbu.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Autowired
    private VodClient vodClient;

    @Override
    public List<EduVideo> selectVideoListByChapterId(String chapterVoId) {

        QueryWrapper<EduVideo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("chapter_id",chapterVoId);

        List<EduVideo> eduVideoList = baseMapper.selectList(queryWrapper);

        return eduVideoList;
    }

    @Override
    public void removeAliyunVideoByCourseId(String courseId) {

        List<String> videoSourceIds = baseMapper.selectVideoSourceIds(courseId);

        String videoIds = "";

        if (videoSourceIds != null && videoSourceIds.size() >0){
            for (int i = 0; i < videoSourceIds.size(); i++) {
                videoIds += videoSourceIds.get(i);
                if (i != videoSourceIds.size() - 1){
                    videoIds += ",";
                }
            }
        }

        vodClient.deleteVideo(videoIds);

        QueryWrapper<EduVideo> videoQueryWrapper = new QueryWrapper<>();
        videoQueryWrapper.eq("course_id",courseId);

        baseMapper.delete(videoQueryWrapper);



    }

    @Override
    public boolean removeByVideoId(String videoId) {

        EduVideo video = baseMapper.selectById(videoId);

        if (video.getVideoSourceId() != null) {
            Result result = vodClient.deleteVideo(video.getVideoSourceId());
            if (result.getCode() == 20001){
                throw new GuliException(20001,"删除视频出错，熔断器...");
            }
        }

        int delete = baseMapper.deleteById(videoId);


        if (delete <= 0){
            return false;
        }else {
            return true;
        }

    }
}
