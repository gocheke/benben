package com.bbu.eduservice.client;

import com.bbu.common.vo.UcenterMember;
import com.bbu.eduservice.client.impl.UcenterClientImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-ucenter",fallback = UcenterClientImpl.class)
@Component
public interface UcenterClient {

    @ApiOperation(value = "根据id获取用户信息")
    @GetMapping("/educenter/member/getUserInfo/{id}")
    public UcenterMember getUserInfo(@PathVariable("id") String id);
}