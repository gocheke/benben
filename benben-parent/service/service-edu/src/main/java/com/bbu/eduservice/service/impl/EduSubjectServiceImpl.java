package com.bbu.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bbu.eduservice.entity.EduSubject;
import com.bbu.eduservice.entity.excel.SubjectData;
import com.bbu.eduservice.entity.subject.OneSubject;
import com.bbu.eduservice.entity.subject.TwoSubject;
import com.bbu.eduservice.listenter.SubjectExcelListener;
import com.bbu.eduservice.mapper.EduSubjectMapper;
import com.bbu.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2022-11-08
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    // 添加课程分类
    @Override
    public void saveSubject(MultipartFile file, EduSubjectService eduSubjectService) {

        try {
            InputStream is = file.getInputStream();

            EasyExcel.read(is, SubjectData.class, new SubjectExcelListener(eduSubjectService)).sheet().doRead();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<OneSubject> getAllOneTwoSubject() {

        // 查询一级分类
        QueryWrapper<EduSubject> oneSubjectQueryWrapper = new QueryWrapper<>();
        oneSubjectQueryWrapper.eq("parent_id","0");

        List<EduSubject> oneList = baseMapper.selectList(oneSubjectQueryWrapper);

        // 查询二级分类
        QueryWrapper<EduSubject> twoSubjectQueryWrapper = new QueryWrapper<>();
        twoSubjectQueryWrapper.ne("parent_id","0");

        List<EduSubject> twoList = baseMapper.selectList(twoSubjectQueryWrapper);

        // 一级分类包装
        ArrayList<OneSubject> oneSubjects = new ArrayList<>();

        for (int i = 0; i < oneList.size(); i++) {
            EduSubject eduSubject = oneList.get(i);
            OneSubject oneSubject = new OneSubject();
            BeanUtils.copyProperties(eduSubject,oneSubject);
            oneSubjects.add(oneSubject);
        }

        // 二级分类包装
        for (int i = 0; i < oneSubjects.size(); i++) {
            OneSubject oneSubject = oneSubjects.get(i);
            ArrayList<TwoSubject> twoSubjects = new ArrayList<>();

            for (int j = 0; j < twoList.size(); j++) {
                TwoSubject twoSubject = new TwoSubject();

                EduSubject twoEduSubject = twoList.get(j);
                if (twoEduSubject.getParentId().equals(oneSubject.getId())){
                    BeanUtils.copyProperties(twoEduSubject,twoSubject);
                    twoSubjects.add(twoSubject);
                }
            }

            oneSubject.setChildren(twoSubjects);
        }

        return oneSubjects;
    }

    @Override
    public List<TwoSubject> getSubject(String id) {

        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("subject_parent_id",id);

        List<EduSubject> eduSubjects = baseMapper.selectList(wrapper);

        ArrayList<TwoSubject> twoSubjects = new ArrayList<>();
        for (int i = 0; i < eduSubjects.size(); i++) {
            EduSubject eduSubject = eduSubjects.get(i);
            TwoSubject twoSubject = new TwoSubject();
            BeanUtils.copyProperties(eduSubject,twoSubject);
            twoSubjects.add(twoSubject);
        }

        return twoSubjects;
    }
}
