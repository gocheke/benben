package com.bbu.eduservice.entity.subject;

import com.bbu.eduservice.entity.EduSubject;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OneSubject{
    private String id;
    private String title;

    private List<TwoSubject> children = new ArrayList<>();
}
