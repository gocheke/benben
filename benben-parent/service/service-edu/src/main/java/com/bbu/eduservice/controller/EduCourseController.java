package com.bbu.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bbu.common.utils.EduConstant;
import com.bbu.common.utils.JwtUtils;
import com.bbu.common.utils.Result;
import com.bbu.eduservice.client.OrderClient;
import com.bbu.eduservice.entity.EduCourse;
import com.bbu.eduservice.entity.chapter.ChapterVo;
import com.bbu.eduservice.entity.chapter.PublishVo;
import com.bbu.eduservice.entity.frontvo.CourseWebVo;
import com.bbu.eduservice.entity.vo.CourseInfoVo;
import com.bbu.eduservice.entity.vo.CourseListVo;
import com.bbu.eduservice.entity.vo.CourseQuery;
import com.bbu.eduservice.mapper.EduCourseMapper;
import com.bbu.eduservice.service.EduChapterService;
import com.bbu.eduservice.service.EduCourseService;
import com.bbu.eduservice.service.EduVideoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/eduservice/course")
public class EduCourseController {

    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduChapterService chapterService;

    @Autowired
    private OrderClient orderClient;

    @GetMapping("getCourseWebInfo/{id}")
    public Result getCourseWebInfo(@PathVariable String id, HttpServletRequest request){

        CourseWebVo courseWebVo = courseService.getCourseWebInfo(id);

        List<ChapterVo> chapterVideoList = chapterService.getChapterVideoById(id);

        String memberId = JwtUtils.getMemberIdByJwtToken(request);

        boolean status = false;

        if (!memberId.isEmpty()) {
             status = orderClient.QueryBuyStatu(id, memberId);
        }

        return Result.ok().data("courseInfo",courseWebVo).data("chapterVideoList",chapterVideoList).data("status",status);
    }

    @DeleteMapping("deleteCourse/{courseId}")
    public Result deleteCourse(@PathVariable String courseId){
        courseService.removeCourse(courseId);
        return Result.ok();
    }

    @PostMapping("getCourseListByCourseQuery/{current}/{limit}")
    public Result getCourseListByCourseQuery(
            @PathVariable("current") Long current,
            @PathVariable("limit") Long limit,
            @RequestBody CourseQuery courseQuery){

        Page<EduCourse> page = new Page<>(current,limit);
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();

        String title = courseQuery.getTitle();
        String status = courseQuery.getStatus();
        String subjectParentId = courseQuery.getSubjectParentId();
        String subjectId = courseQuery.getSubjectId();
        String teacherId = courseQuery.getTeacherId();
        String begin = courseQuery.getBegin();
        String end = courseQuery.getEnd();

//        List<CourseListVo> coursePage = courseService.getCoursePage(title, status, subjectParentId, subjectId, teacherId, begin, end, current, limit);
//
//        int total = courseService.count(null);
        if (!StringUtils.isEmpty(title)){
            wrapper.like("title",title);
        }
        if (!StringUtils.isEmpty(status)){
            wrapper.eq("status",status);
        }
        if (!StringUtils.isEmpty(subjectParentId)){
            wrapper.eq("subject_parent_id",subjectParentId);
        }
        if (!StringUtils.isEmpty(subjectId)){
            wrapper.eq("subject_id",subjectId);
        }
        if (!StringUtils.isEmpty(teacherId)){
            wrapper.eq("teacher_id",teacherId);
        }

        if (!StringUtils.isEmpty(begin)) {
            wrapper.ge("gmt_create", begin);
        }

        if (!StringUtils.isEmpty(end)) {
            wrapper.le("gmt_create", end);
        }

        IPage<EduCourse> courseIPage = courseService.page(page, wrapper);
        List<EduCourse> coursePage = courseIPage.getRecords();
        long total = courseIPage.getTotal();

        return Result.ok().data("total",total).data("courseList",coursePage);
    }

    @GetMapping("getAllCourseList")
    public Result getAllCourseList(){

        List<CourseListVo> list = courseService.getAllCourse();

        return Result.ok().data("list",list);
    }

    // 添加课程基本信息
    @PostMapping("addCourseInfo")
    public Result addCourseInfo(@RequestBody CourseInfoVo courseInfoVo){

        String id = courseService.saveCourseInfo(courseInfoVo);

        return Result.ok().data("courseId",id);
    }

    @GetMapping("getCourseInfo/{courseId}")
    public Result getCourseInfo(@PathVariable("courseId") String courseId){

        CourseInfoVo courseInfoVo = courseService.getCourseInfoById(courseId);

        return Result.ok().data("courseInfoVo",courseInfoVo);
    }

    @PostMapping("updateCourseInfo")
    public Result updateCourseInfo(@RequestBody CourseInfoVo courseInfoVo){

        courseService.updateCourseInfoById(courseInfoVo);

        return Result.ok();
    }

    @GetMapping("getAllPublish/{courseId}")
    public Result getAllPublish(@PathVariable("courseId") String courseId){

        PublishVo publishVo = courseService.getPublishInfo(courseId);

        return Result.ok().data("publishVo",publishVo);
    }

    @PostMapping("publishCourse/{courseId}")
    public Result publishCourse(@PathVariable String courseId){

        EduCourse course = new EduCourse();
        course.setId(courseId);
        course.setStatus(EduConstant.COURSE_STATUS_NORMAL);

        courseService.updateById(course);

        return Result.ok();
    }

}

