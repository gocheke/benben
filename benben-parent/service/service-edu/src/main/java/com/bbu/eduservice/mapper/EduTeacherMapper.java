package com.bbu.eduservice.mapper;

import com.bbu.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2022-10-31
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
