package com.bbu.eduservice.entity.chapter;

import lombok.Data;

@Data
public class VideoVo {

    private String id;

    private String ChapterId;

    private String title;

    private String videoSourceId;
}
