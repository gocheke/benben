package com.bbu.eduservice.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.Period;
import java.util.Date;

@Data
public class CourseQuery implements Serializable {

    private static final long serialVersionUID = -1041316766951335167L;

    // 课程名称
    private String title;

    // 课程状态
    private String status;

    // 一级分类Id
    private String subjectParentId;

    // 二级分类Id
    private String subjectId;

    // 讲师Id
    private String teacherId;

    // 开始时间
    @ApiModelProperty(value = "查询开始时间", example = "2019-01-01 10:10:10")
    private String begin;

    // 截止时间
    @ApiModelProperty(value = "查询结束时间", example = "2019-12-01 10:10:10")
    private String end;

}
