package com.bbu.eduservice.service;

import com.bbu.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bbu.eduservice.entity.subject.OneSubject;
import com.bbu.eduservice.entity.subject.TwoSubject;
import org.springframework.web.multipart.MultipartFile;

import javax.security.auth.Subject;
import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author testjava
 * @since 2022-11-08
 */
public interface EduSubjectService extends IService<EduSubject> {

    void saveSubject(MultipartFile file,EduSubjectService eduSubjectService);

    List<OneSubject> getAllOneTwoSubject();

    List<TwoSubject> getSubject(String id);
}
