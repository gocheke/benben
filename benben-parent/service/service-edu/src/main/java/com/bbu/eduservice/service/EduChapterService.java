package com.bbu.eduservice.service;

import com.bbu.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bbu.eduservice.entity.chapter.ChapterVo;
import com.bbu.eduservice.entity.vo.CourseInfoVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
public interface EduChapterService extends IService<EduChapter> {

    List<ChapterVo> getChapterVideoById(String id);

    boolean deleteById(String chapterId);
}
