package com.bbu.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bbu.eduservice.entity.*;
import com.bbu.eduservice.entity.chapter.PublishVo;
import com.bbu.eduservice.entity.frontvo.CourseFrontVo;
import com.bbu.eduservice.entity.frontvo.CourseWebVo;
import com.bbu.eduservice.entity.vo.CourseInfoVo;
import com.bbu.eduservice.entity.vo.CourseListVo;
import com.bbu.eduservice.mapper.EduCourseMapper;
import com.bbu.eduservice.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bbu.servicebase.exceptionhandler.GuliException;
import io.swagger.models.auth.In;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseDescriptionService courseDescriptionService;

    @Autowired
    private EduSubjectService eduSubjectService;

    @Autowired
    private EduChapterService chapterService;

    @Autowired
    private EduVideoService videoService;

    @Autowired
    private EduTeacherService eduTeacherService;

    @Override
    public String saveCourseInfo(CourseInfoVo courseInfoVo) {
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        int insert = baseMapper.insert(eduCourse);

        if (insert <= 0){
            throw new GuliException(20001,"添加课程信息失败");
        }

        String id = eduCourse.getId();

        EduCourseDescription courseDescription = new EduCourseDescription();
        courseDescription.setDescription(courseInfoVo.getDescription());
        courseDescription.setId(id);

        courseDescriptionService.save(courseDescription);

        return id;
    }

    @Override
    public CourseInfoVo getCourseInfoById(String courseId) {

        EduCourse eduCourse = baseMapper.selectById(courseId);
        EduCourseDescription eduCourseDescription = courseDescriptionService.getById(courseId);

        CourseInfoVo courseInfoVo = new CourseInfoVo();
        BeanUtils.copyProperties(eduCourse,courseInfoVo);

        courseInfoVo.setDescription(eduCourseDescription.getDescription());

        return courseInfoVo;
    }

    @Override
    public void updateCourseInfoById(CourseInfoVo courseInfoVo) {

        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        String id = courseInfoVo.getId();
        QueryWrapper<EduCourse> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id",id);

        String description = courseInfoVo.getDescription();

        EduCourseDescription eduCourseDescription = new EduCourseDescription();

        eduCourseDescription.setId(id);
        eduCourseDescription.setDescription(description);

        boolean update = courseDescriptionService.updateById(eduCourseDescription);

        if (!update){
            throw new GuliException(20001,"修改课程简介失败");
        }

        int flag = baseMapper.update(eduCourse, queryWrapper);

        if (flag == 0){
            throw new GuliException(20001,"修改课程信息失败");
        }

    }

    @Override
    public PublishVo getPublishInfo(String courseId) {

        // 多次查询数据库，效率差，编写SQL一次性查完
//        PublishVo publishVo = new PublishVo();

//        EduCourse eduCourse = baseMapper.selectById(courseId);
//        publishVo.setCover(eduCourse.getCover());
//        publishVo.setTitle(eduCourse.getTitle());
//        publishVo.setLessonNum(eduCourse.getLessonNum());
//
//        EduSubject subjectParentName = eduSubjectService.getById(eduCourse.getSubjectParentId());
//        EduSubject subjectName = eduSubjectService.getById(eduCourse.getSubjectId());
//
//        publishVo.setSubjectLevelOne(subjectParentName.getTitle());
//        publishVo.setSubjectLevelTwo(subjectName.getTitle());
//
//        EduTeacher eduTeacher = eduTeacherService.getById(eduCourse.getTeacherId());
//        publishVo.setTeacherName(eduTeacher.getName());
//
//        publishVo.setPrice(eduCourse.getPrice());

        PublishVo publishVo = baseMapper.selectPublishByCourseId(courseId);

        return publishVo;
    }

    @Override
    public List<CourseListVo> getAllCourse() {

        List<CourseListVo> courseListVos = baseMapper.selectAllCourse();

        return courseListVos;
    }

    @Override
    public List<CourseListVo> getCoursePage(String title,String status,String subjectParentId,
                                            String subjectId,String teacherId,String begin,String end,
                                            Long current,Long limit) {

        List<CourseListVo> courseListVos = baseMapper.selectCoursePage(title, status, subjectParentId, subjectId, teacherId, begin, end, current, limit);

        return courseListVos;
    }

    @Override
    public void removeCourse(String courseId) {
        baseMapper.deleteById(courseId);

        QueryWrapper<EduChapter> chapterQueryWrapper = new QueryWrapper<>();
        chapterQueryWrapper.eq("course_id",courseId);

        chapterService.remove(chapterQueryWrapper);


        videoService.removeAliyunVideoByCourseId(courseId);
//        QueryWrapper<EduVideo> videoQueryWrapper = new QueryWrapper<>();
//        videoQueryWrapper.eq("course_id",courseId);

//        videoService.remove(videoQueryWrapper);
    }

    @Override
    public Map<String, Object> getCourseFrontList(Page<EduCourse> coursePage, CourseFrontVo courseFrontVo) {

        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();

        if (!StringUtils.isEmpty(courseFrontVo.getSubjectId())){
            wrapper.eq("subject_id",courseFrontVo.getSubjectId());
        }

        if(!StringUtils.isEmpty(courseFrontVo.getSubjectParentId())){
            wrapper.eq("subject_parent_id",courseFrontVo.getSubjectParentId());
        }

        if (!StringUtils.isEmpty(courseFrontVo.getBuyCountSort())){
            wrapper.orderByDesc("buy_count");
        }

        if (!StringUtils.isEmpty(courseFrontVo.getGmtCreateSort())){
            wrapper.orderByDesc("gmt_create");
        }

        if (!StringUtils.isEmpty(courseFrontVo.getPriceSort())){
            wrapper.orderByDesc("price");
        }

        if (!StringUtils.isEmpty(courseFrontVo.getTitle())){
            wrapper.like("title",courseFrontVo.getTitle());
        }

        baseMapper.selectPage(coursePage, wrapper);

        List<EduCourse> list = coursePage.getRecords();
        long current = coursePage.getCurrent();
        long size = coursePage.getSize();
        long total = coursePage.getTotal();
        boolean hasNext = coursePage.hasNext();
        boolean hasPrevious = coursePage.hasPrevious();
        long pages = coursePage.getPages();

        HashMap<String, Object> map = new HashMap<>();

        map.put("list",list);
        map.put("pages",pages);
        map.put("current",current);
        map.put("size",size);
        map.put("total",total);
        map.put("hasNext",hasNext);
        map.put("hasPrevious",hasPrevious);

        return map;
    }

    @Override
    public CourseWebVo getCourseWebInfo(String id) {

        CourseWebVo courseWebVo = baseMapper.getCourseWebInfoById(id);

        return courseWebVo;
    }
}
