package com.bbu.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bbu.common.utils.Result;
import com.bbu.eduservice.entity.EduChapter;
import com.bbu.eduservice.entity.chapter.ChapterVo;
import com.bbu.eduservice.entity.vo.CourseInfoVo;
import com.bbu.eduservice.service.EduChapterService;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
@RestController
@RequestMapping("/eduservice/chapter")
public class EduChapterController {

    @Autowired
    EduChapterService eduChapterService;

    @GetMapping("/getChapterVideo/{courseId}")
    public Result getChapterVideo(@PathVariable("courseId") String id){

        List<ChapterVo> chapterVoList = eduChapterService.getChapterVideoById(id);

        return Result.ok().data("chapterList",chapterVoList);
    }

    @PostMapping("addChapter")
    public Result addChapter(@RequestBody EduChapter chapter){

        eduChapterService.save(chapter);

        return Result.ok();
    }

    @GetMapping("getChapterInfo/{chapterId}")
    public Result getChapterInfo(@PathVariable("chapterId") String chapterId){

        QueryWrapper<EduChapter> wrapper = new QueryWrapper<>();
        wrapper.eq("id",chapterId);

        EduChapter eduChapter = eduChapterService.getOne(wrapper);

        return Result.ok().data("eduChapter",eduChapter);
    }

    @PostMapping("updateChapterInfo")
    public Result updateChapterInfo(@RequestBody EduChapter chapter){

        eduChapterService.updateById(chapter);

        return Result.ok();
    }

    @DeleteMapping("deleteChapter/{chapterId}")
    public Result deleteChapter(@PathVariable String chapterId){

        boolean delete = eduChapterService.deleteById(chapterId);

        if (delete) {
            return Result.ok();
        }

        return Result.error().message("此章节包含小节，无法删除");
    }

}

