package com.bbu.eduservice.listenter;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bbu.common.utils.Result;
import com.bbu.common.utils.ResultCode;
import com.bbu.eduservice.entity.EduSubject;
import com.bbu.eduservice.entity.excel.SubjectData;
import com.bbu.eduservice.service.EduSubjectService;
import com.bbu.servicebase.exceptionhandler.GuliException;

import java.util.Map;

public class SubjectExcelListener extends AnalysisEventListener<SubjectData> {

    // 这个listener需要手动new出来，不能交给ioc容器，所以通过构造器注入service
    private EduSubjectService eduSubjectService;

    public SubjectExcelListener() {
    }

    public SubjectExcelListener(EduSubjectService eduSubjectService) {
        this.eduSubjectService = eduSubjectService;
    }

    @Override
    public void invoke(SubjectData subjectData, AnalysisContext analysisContext) {
//        System.out.println("监听器invoke执行了");
        if (subjectData == null){
            throw new GuliException(ResultCode.ERROR,"文件数据为空");
        }

        EduSubject eduOneSubject = existOneEduSubject(eduSubjectService, subjectData.getOneSubjectName());

        if (eduOneSubject == null){
            eduOneSubject = new EduSubject();
            eduOneSubject.setTitle(subjectData.getOneSubjectName());
            eduOneSubject.setParentId("0");
            eduSubjectService.save(eduOneSubject);
        }

        String pid = eduOneSubject.getId();

        EduSubject eduTwoSubject = existTwoEduSubject(eduSubjectService, subjectData.getTwoSubjectName(), pid);

        if (eduTwoSubject == null){
            eduTwoSubject = new EduSubject();
            eduTwoSubject.setTitle(subjectData.getTwoSubjectName());
            eduTwoSubject.setParentId(pid);
            eduSubjectService.save(eduTwoSubject);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

    // 判断一级分类不能重复添加
    private EduSubject existOneEduSubject(EduSubjectService subjectService ,String name){

        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();

        wrapper.eq("title",name);
        wrapper.eq("parent_id",0);

        EduSubject eduSubject = eduSubjectService.getOne(wrapper);

        return eduSubject;
    }

    // 判断二级分类不能重复添加
    private EduSubject existTwoEduSubject(EduSubjectService subjectService ,String name,String pid){

        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();

        wrapper.eq("title",name);
        wrapper.eq("parent_id",pid);

        EduSubject eduSubject = eduSubjectService.getOne(wrapper);

        return eduSubject;
    }
}
