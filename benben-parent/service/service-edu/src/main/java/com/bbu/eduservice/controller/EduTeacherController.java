package com.bbu.eduservice.controller;


import com.bbu.common.utils.Result;
import com.bbu.common.utils.ResultCode;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bbu.eduservice.entity.EduCourse;
import com.bbu.eduservice.entity.EduTeacher;
import com.bbu.eduservice.entity.vo.TeacherQuery;
import com.bbu.eduservice.service.EduCourseService;
import com.bbu.eduservice.service.EduTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2022-10-31
 */
@Api(description="讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
public class EduTeacherController {

    @Autowired
    private EduTeacherService teacherService;

    @Autowired
    private EduCourseService courseService;

    // 1.查询讲师表所有数据
    @GetMapping("/findAll")
    @ApiOperation(value = "所有讲师列表")
    public Result findAllTeacher(){

        List<EduTeacher> list = teacherService.list(null);
        return Result.ok().code(ResultCode.SUCCESS).data("items",list);
    }

    // 2.讲师逻辑删除功能
    @DeleteMapping("{id}")
    @ApiOperation(value = "根据ID逻辑删除讲师")
    public Result removeById(
            @ApiParam(name = "id", value = "讲师ID", required = true)
            @PathVariable String id){

        boolean flag = teacherService.removeById(id);

        if (flag) {
            return Result.ok().code(ResultCode.SUCCESS);
        }else {
            return Result.error().code(ResultCode.ERROR);
        }
    }

    @ApiOperation(value = "分页讲师列表")
    @GetMapping("pageTeacher/{current}/{limit}")
    public Result pageList(
            @ApiParam(name = "current", value = "当前页码", required = true)
            @PathVariable("current") Long current,

            @ApiParam(name = "limit", value = "每页记录数", required = true)

            @PathVariable("limit") Long limit){

        Page<EduTeacher> pageParam = new Page<>(current,limit);

        teacherService.page(pageParam, null);

        List<EduTeacher> records = pageParam.getRecords();

        Long total = pageParam.getTotal();

        return Result.ok().code(ResultCode.SUCCESS).data("total",total).data("rows",records);
    }

    @ApiOperation(value = "条件查询带分页的讲师")
    @PostMapping("pageTeacherCondition/{current}/{limit}")
    public Result pageTeacherCondition(
            @PathVariable Long current,
            @PathVariable Long limit,
            @RequestBody(required = false) TeacherQuery teacherQuery){

        Page<EduTeacher> pageParam = new Page<>(current,limit);

        QueryWrapper<EduTeacher> queryWrapper = new QueryWrapper<>();

        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();

        if (!StringUtils.isEmpty(name)) {
            queryWrapper.like("name", name);
        }

        if (!StringUtils.isEmpty(level) ) {
            queryWrapper.eq("level", level);
        }

        if (!StringUtils.isEmpty(begin)) {
            queryWrapper.ge("gmt_create", begin);
        }

        if (!StringUtils.isEmpty(end)) {
            queryWrapper.le("gmt_create", end);
        }

        queryWrapper.orderByDesc("gmt_create");
        teacherService.page(pageParam, queryWrapper);

        List<EduTeacher> records = pageParam.getRecords();

        Long total = pageParam.getTotal();

        return Result.ok().code(ResultCode.SUCCESS).data("total",total).data("rows",records);
    }

    @ApiOperation(value = "新增讲师")
    @PostMapping("addTeacher")
    public Result save(
            @ApiParam(name = "teacher", value = "讲师对象", required = true)
            @RequestBody EduTeacher eduTeacher){

        boolean save = teacherService.save(eduTeacher);

        if (save){
            return Result.ok().code(ResultCode.SUCCESS);
        }else {
            return Result.error().code(ResultCode.ERROR);
        }
    }

    @ApiOperation(value = "根据讲师id进行查询")
    @GetMapping("getTeacher/{id}")
    public Result getTeacher (@PathVariable Long id){
        EduTeacher teacher = teacherService.getById(id);
        return Result.ok().code(ResultCode.SUCCESS).data("teacher",teacher);
    }

    @ApiOperation(value = "根据ID修改讲师")
    @PostMapping("updateTeacher")
    public Result updateById(
            @RequestBody EduTeacher teacher){

        teacherService.updateById(teacher);
        return Result.ok();
    }

    @GetMapping("getTeacherInfo/{id}")
    public Result getTeacherInfo(@PathVariable String id){

        EduTeacher teacher = teacherService.getById(id);

        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();

        wrapper.eq("teacher_id",id);
        wrapper.last("limit 0,4");
        List<EduCourse> course = courseService.list(wrapper);

        return Result.ok().data("teacher",teacher).data("courseList",course);
    }

}
