package com.bbu.eduservice.client.impl;

import com.bbu.common.utils.Result;
import com.bbu.eduservice.client.VodClient;
import org.springframework.stereotype.Component;

@Component
public class VodClientImpl implements VodClient {
    @Override
    public Result deleteVideo(String id) {
        return Result.error().message("删除视频出错了");
    }
}
