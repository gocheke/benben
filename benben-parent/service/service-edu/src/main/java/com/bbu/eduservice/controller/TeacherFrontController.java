package com.bbu.eduservice.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bbu.common.utils.Result;
import com.bbu.eduservice.entity.EduTeacher;
import com.bbu.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("eduservice/teacherfront")
public class TeacherFrontController {

    @Autowired
    private EduTeacherService teacherService;

    @GetMapping("getTeacherFrontList/{page}/{limit}")
    public Result getTeacherFrontList(@PathVariable Long page,@PathVariable Long limit){

        Page<EduTeacher> pageTeacher = new Page<EduTeacher>(page,limit);

        Map<String,Object> map = teacherService.getTeacherList(pageTeacher);

        return Result.ok().data(map);
    }
}
