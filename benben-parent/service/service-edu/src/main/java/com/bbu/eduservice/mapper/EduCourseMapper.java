package com.bbu.eduservice.mapper;

import com.bbu.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bbu.eduservice.entity.chapter.PublishVo;
import com.bbu.eduservice.entity.frontvo.CourseWebVo;
import com.bbu.eduservice.entity.vo.CourseListVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
public interface EduCourseMapper extends BaseMapper<EduCourse> {

    PublishVo selectPublishByCourseId(String courseId);

    List<CourseListVo> selectAllCourse();

    List<CourseListVo> selectCoursePage(
            @Param("title") String title,
            @Param("status") String status,
            @Param("subjectParentId") String subjectParentId,
            @Param("subjectId") String subjectId,
            @Param("teacherId") String teacherId,
            @Param("begin") String begin,
            @Param("end") String end,
            @Param("current") Long current,
            @Param("limit") Long limit
    );


    CourseWebVo getCourseWebInfoById(String id);
}
