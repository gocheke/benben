package com.bbu.eduservice.client;

import com.bbu.eduservice.client.impl.OrderClientImpl;
import com.bbu.eduservice.client.impl.UcenterClientImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-order",fallback = OrderClientImpl.class)
@Component
public interface OrderClient {

    @GetMapping("/eduorder/order/QueryBuyStatu/{courseId}/{memberId}")
    public boolean QueryBuyStatu(@PathVariable String courseId , @PathVariable String memberId);

}
