package com.bbu.eduservice.mapper;

import com.bbu.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
