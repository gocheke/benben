package com.bbu.eduservice.mapper;

import com.bbu.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
public interface EduVideoMapper extends BaseMapper<EduVideo> {

    List<String> selectVideoSourceIds(String courseId);

}
