package com.bbu.eduservice.controller;


import com.bbu.common.utils.Result;
import com.bbu.eduservice.entity.subject.OneSubject;
import com.bbu.eduservice.entity.subject.TwoSubject;
import com.bbu.eduservice.service.EduSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2022-11-08
 */
@RestController
@RequestMapping("/eduservice/subject")
public class EduSubjectController {

    @Autowired
    EduSubjectService eduSubjectService;

    @PostMapping("addSubject")
    public Result addSubject(MultipartFile file){

        eduSubjectService.saveSubject(file,eduSubjectService);

        return Result.ok();
    }

    @GetMapping("getAllSubject")
    public Result getAllSubject(){
        List<OneSubject> list = eduSubjectService.getAllOneTwoSubject();
        return Result.ok().data("list",list);
    }

    @GetMapping("getSubjectById/{id}")
    public Result getSubjectById(@PathVariable("id") String id){
        List<TwoSubject> list = eduSubjectService.getSubject(id);
        return Result.ok().data("list",list);
    }

}

