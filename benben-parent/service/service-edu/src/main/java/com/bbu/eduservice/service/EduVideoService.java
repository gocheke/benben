package com.bbu.eduservice.service;

import com.bbu.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
public interface EduVideoService extends IService<EduVideo> {

    List<EduVideo> selectVideoListByChapterId(String chapterVoId);

    void removeAliyunVideoByCourseId(String courseId);

    boolean removeByVideoId(String videoId);
}
