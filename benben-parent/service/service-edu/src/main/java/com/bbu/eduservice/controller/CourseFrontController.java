package com.bbu.eduservice.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bbu.common.utils.Result;
import com.bbu.common.vo.CourseWebVoOrder;
import com.bbu.eduservice.entity.EduCourse;
import com.bbu.eduservice.entity.frontvo.CourseFrontVo;
import com.bbu.eduservice.entity.frontvo.CourseWebVo;
import com.bbu.eduservice.service.EduCourseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/eduservice/coursefront")
public class CourseFrontController {

    @Autowired
    private EduCourseService courseService;

    @PostMapping("getCourseInfo/{id}")
    public CourseWebVoOrder getCourseInfo(@PathVariable String id){

        CourseWebVo courseWebInfo = courseService.getCourseWebInfo(id);
        CourseWebVoOrder courseWebVoOrder = new CourseWebVoOrder();
        BeanUtils.copyProperties(courseWebInfo,courseWebVoOrder);

        return courseWebVoOrder;
    }

    @PostMapping("getFrontCourseList/{page}/{limit}")
    public Result getFrontCourseList(@PathVariable Long page, @PathVariable Long limit,
                                     @RequestBody(required = false) CourseFrontVo courseFrontVo){

        Page<EduCourse> coursePage = new Page<>(page,limit);

        Map<String,Object> map = courseService.getCourseFrontList(coursePage,courseFrontVo);

        return Result.ok().data(map);
    }

}
