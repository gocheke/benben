package com.bbu.eduservice.client.impl;

import com.bbu.common.vo.UcenterMember;
import com.bbu.eduservice.client.UcenterClient;
import com.bbu.servicebase.exceptionhandler.GuliException;
import org.springframework.stereotype.Component;

@Component
public class UcenterClientImpl implements UcenterClient {

    @Override
    public UcenterMember getUserInfo(String id) {
        throw new GuliException(20001,"获取会员信息失败");
    }
}
