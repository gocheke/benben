package com.bbu.eduservice.service;

import com.bbu.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author testjava
 * @since 2022-11-12
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
