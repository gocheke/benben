package com.bbu.eduservice.client;

import com.bbu.common.utils.Result;
import com.bbu.eduservice.client.impl.VodClientImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-vod",fallback = VodClientImpl.class)
@Component
public interface VodClient {

    @DeleteMapping("/eduvod/video/deleteVideo/{id}")
    public Result deleteVideo(@PathVariable("id") String id);

}
