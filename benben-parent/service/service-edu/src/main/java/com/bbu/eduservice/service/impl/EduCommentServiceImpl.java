package com.bbu.eduservice.service.impl;

import com.bbu.eduservice.entity.EduComment;
import com.bbu.eduservice.mapper.EduCommentMapper;
import com.bbu.eduservice.service.EduCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2022-11-29
 */
@Service
public class EduCommentServiceImpl extends ServiceImpl<EduCommentMapper, EduComment> implements EduCommentService {

}
