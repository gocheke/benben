package com.bbu.demo.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Map;

public class ExcelLister extends AnalysisEventListener<DemoData> {


    @Override
    public void invokeHeadMap(Map headMap, AnalysisContext context) {
        System.out.println(headMap);
    }

    @Override
    public void invoke(DemoData demoData, AnalysisContext analysisContext) {
        System.out.println(demoData);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
