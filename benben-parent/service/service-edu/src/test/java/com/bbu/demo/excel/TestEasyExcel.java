package com.bbu.demo.excel;

import com.alibaba.excel.EasyExcel;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

public class TestEasyExcel {

    @Test
    public void test01(){

        // 实现excel写操作
//        String filePath = "E:\\Desktop\\01.xlsx";
//
//        EasyExcel.write(filePath,DemoData.class).sheet("学生列表").doWrite(getData());

        String filePath = "E:\\Desktop\\01.xlsx";

        // 实现excel读操作
        EasyExcel.read(filePath,DemoData.class,new ExcelLister()).sheet().doRead();


    }


    public List<DemoData> getData(){
        ArrayList<DemoData> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            list.add(new DemoData(i,"name"+i));
        }

        return list;
    }
}
