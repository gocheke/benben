package com.bbu.statistics.controller;


import com.bbu.common.utils.Result;
import com.bbu.statistics.client.UcentClient;
import com.bbu.statistics.service.StatisticsDailyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2022-12-05
 */
@RestController
@RequestMapping("/stasevice/sta")
public class StatisticsDailyController {

    @Autowired
    private StatisticsDailyService staService;

    @GetMapping("registerCount/{day}")
    public Result registerCount(@PathVariable String day){

        staService.registerCount(day);

        return Result.ok();
    }

    // 图表显示，返回两部分数据，日期json数组，数量json数组
    @GetMapping("showData/{type}/{begin}/{end}")
    public Result showData(@PathVariable String type,@PathVariable String begin,@PathVariable String end){

        Map<String,Object> map = staService.getShowData(type,begin,end);

        return Result.ok().data(map);
    }

}

