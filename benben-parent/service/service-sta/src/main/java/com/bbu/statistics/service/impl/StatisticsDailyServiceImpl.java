package com.bbu.statistics.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bbu.common.utils.Result;
import com.bbu.statistics.client.UcentClient;
import com.bbu.statistics.entity.StatisticsDaily;
import com.bbu.statistics.mapper.StatisticsDailyMapper;
import com.bbu.statistics.service.StatisticsDailyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2022-12-05
 */
@Service
public class StatisticsDailyServiceImpl extends ServiceImpl<StatisticsDailyMapper, StatisticsDaily> implements StatisticsDailyService {

    @Autowired
    private UcentClient ucentClient;

    @Override
    public void registerCount(String day) {

        // 添加之前删除已存在的数据
        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();

        wrapper.eq("date_calculated",day);
        baseMapper.delete(wrapper);

        Result result = ucentClient.countRegister(day);
        Map map = result.getData();
        Integer count = (Integer) map.get("registerCount");

        StatisticsDaily sta = new StatisticsDaily();

        sta.setRegisterNum(count); // 注册人数
        sta.setDateCalculated(day); // 统计日期
        sta.setVideoViewNum(RandomUtils.nextInt(100,200));
        sta.setLoginNum(RandomUtils.nextInt(100,200));
        sta.setCourseNum(RandomUtils.nextInt(100,200));

        baseMapper.insert(sta);
    }

    @Override
    public Map<String, Object> getShowData(String type, String begin, String end) {

        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();
        wrapper.between("date_calculated",begin,end);
        wrapper.select("date_calculated",type);

        List<StatisticsDaily> staList = baseMapper.selectList(wrapper);

        // 因为返回又两部份数据：日期 和 日期对应数量
        // 前端要求数组json结构，对应后端java代码是list集合
        // 创建两个list集合，一个日期list，一个数量list
        List<String> date_calculatedList = new ArrayList<>();
        List<Integer> numDataList = new ArrayList<>();

        // 遍历查询所有数据list集合，进行封装
        for (int i = 0; i < staList.size(); i++) {
            StatisticsDaily daily = staList.get(i);

            date_calculatedList.add(daily.getDateCalculated());

            // 封装对应数量
            switch (type){
                case "login_num":
                    numDataList.add(daily.getLoginNum());
                    break;
                case "register_num":
                    numDataList.add(daily.getRegisterNum());
                    break;
                case "video_view_num":
                    numDataList.add(daily.getVideoViewNum());
                    break;
                case "course_num":
                    numDataList.add(daily.getCourseNum());
                    break;
                default :
                    break;
            }
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("date_calculatedList",date_calculatedList);
        map.put("numDataList",numDataList);

        return map;
    }
}
