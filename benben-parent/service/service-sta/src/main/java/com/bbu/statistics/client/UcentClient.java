package com.bbu.statistics.client;

import com.bbu.common.utils.Result;
import com.bbu.statistics.client.impl.UcentClientImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-ucenter",fallback = UcentClientImpl.class)
@Component
public interface UcentClient {

    @GetMapping("/educenter/member/countRegister/{day}")
    public Result countRegister(@PathVariable String day);
}
