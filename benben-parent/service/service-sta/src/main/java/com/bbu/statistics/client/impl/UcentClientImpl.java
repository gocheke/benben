package com.bbu.statistics.client.impl;

import com.bbu.common.utils.Result;
import com.bbu.servicebase.exceptionhandler.GuliException;
import com.bbu.statistics.client.UcentClient;
import org.springframework.stereotype.Component;

@Component
public class UcentClientImpl implements UcentClient {
    @Override
    public Result countRegister(String day) {
        throw new GuliException(20001,"远程获取注册人数异常");
    }
}
