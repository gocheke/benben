package com.bbu.educenter.mapper;

import com.bbu.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2022-11-22
 */
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {

    Integer countRegisterByDay(String day);
}
