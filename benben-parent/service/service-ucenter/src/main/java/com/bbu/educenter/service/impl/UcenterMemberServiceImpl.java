package com.bbu.educenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bbu.common.utils.JwtUtils;
import com.bbu.common.utils.MD5;
import com.bbu.educenter.entity.UcenterMember;
import com.bbu.educenter.entity.vo.RegisterVo;
import com.bbu.educenter.mapper.UcenterMemberMapper;
import com.bbu.educenter.service.UcenterMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bbu.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2022-11-22
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public String login(UcenterMember ucenterMember) {

        String mobile = ucenterMember.getMobile();
        String password = ucenterMember.getPassword();

        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)){
            throw new GuliException(20001,"登陆失败");
        }

        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);

        UcenterMember mobileMember = baseMapper.selectOne(wrapper);

        if (mobileMember == null){
            throw new GuliException(20001,"登陆失败");
        }

        password = MD5.encrypt(password);

        if (!mobileMember.getPassword().equals(password)){
            throw new GuliException(20001,"登陆失败");
        }

        if (mobileMember.getIsDisabled()){
            throw new GuliException(20001,"登陆失败");
        }

        String jwtToken = JwtUtils.getJwtToken(mobileMember.getId(), mobileMember.getNickname());


        return jwtToken;
    }

    @Override
    public void register(RegisterVo registerVo) {
        String mobile = registerVo.getMobile();
        String nickname = registerVo.getNickname();
        String password = registerVo.getPassword();
        String code = registerVo.getCode();

        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password) || StringUtils.isEmpty(nickname) || StringUtils.isEmpty(code)){
            throw new GuliException(20001,"注册失败");
        }

        String redisCode = redisTemplate.opsForValue().get(mobile);

        if (!redisCode.equals(code)){
            throw new GuliException(20001,"注册失败");
        }

        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();

        wrapper.eq("mobile",mobile);
        Integer count = baseMapper.selectCount(wrapper);

        if (count > 0){
            throw new GuliException(20001,"注册失败");
        }

        UcenterMember member = new UcenterMember();
        member.setMobile(mobile);
        member.setNickname(nickname);
        member.setPassword(MD5.encrypt(password));
        member.setIsDisabled(false);
        member.setAvatar("http://aliyun_id_photo_bucket.oss.aliyuncs.com/default_handsome.jpg");

        baseMapper.insert(member);

    }

    @Override
    public UcenterMember getByOpenid(String openid) {

        QueryWrapper<UcenterMember> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("openid",openid);

        UcenterMember member = baseMapper.selectOne(queryWrapper);

        return member;
    }

    @Override
    public Integer countRegisterDay(String day) {

        Integer count = baseMapper.countRegisterByDay(day);

        return count;
    }
}
