package com.bbu.educenter.service;

import com.bbu.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bbu.educenter.entity.vo.RegisterVo;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author testjava
 * @since 2022-11-22
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    String login(UcenterMember ucenterMember);

    void register(RegisterVo registerVo);

    UcenterMember getByOpenid(String openid);

    Integer countRegisterDay(String day);
}
