package com.bbu.educenter.controller;


import com.baomidou.mybatisplus.extension.api.R;
import com.bbu.common.utils.JwtUtils;
import com.bbu.common.utils.Result;
import com.bbu.common.vo.UcenterMemberOrder;
import com.bbu.educenter.entity.UcenterMember;
import com.bbu.educenter.entity.vo.RegisterVo;
import com.bbu.educenter.service.UcenterMemberService;
import com.bbu.servicebase.exceptionhandler.GuliException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2022-11-22
 */
@RestController
@RequestMapping("/educenter/member")
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService ucenterMemberService;

    // 查询某天的注册人数
    @GetMapping("countRegister/{day}")
    public Result countRegister(@PathVariable String day){

        Integer count = ucenterMemberService.countRegisterDay(day);

        return Result.ok().data("registerCount",count);
    }

    // 根据用户id查询用户信息
    @PostMapping("getUserInfoOrder/{id}")
    public UcenterMemberOrder getUserInfoOrder(@PathVariable String id){

        UcenterMember member = ucenterMemberService.getById(id);
        UcenterMemberOrder memberOrder = new UcenterMemberOrder();

        BeanUtils.copyProperties(member,memberOrder);

        return memberOrder;
    }

    @GetMapping("getUserInfo/{id}")
    public com.bbu.common.vo.UcenterMember getUserInfo(@PathVariable("id") String id){
        UcenterMember member = ucenterMemberService.getById(id);
        com.bbu.common.vo.UcenterMember ucenterMember = new com.bbu.common.vo.UcenterMember();
        BeanUtils.copyProperties(member,ucenterMember);
        return ucenterMember;
    }

    @PostMapping("login")
    public Result login(@RequestBody UcenterMember ucenterMember){

        String token = ucenterMemberService.login(ucenterMember);

        return Result.ok().data("token",token);
    }

    @PostMapping("register")
    public Result registerUser(@RequestBody RegisterVo registerVo){

        ucenterMemberService.register(registerVo);

        return Result.ok();

    }

    @ApiOperation(value = "根据token获取登录信息")
    @GetMapping("auth/getLoginInfo")
    public Result getLoginInfo(HttpServletRequest request){
        try {
                String memberId = JwtUtils.getMemberIdByJwtToken(request);
            UcenterMember member = ucenterMemberService.getById(memberId);
            return Result.ok().data("item", member);
        }catch (Exception e){
            e.printStackTrace();
            throw new GuliException(20001,"error");
        }
    }

}

