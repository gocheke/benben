package com.bbu.cms.entity.vo;

import lombok.Data;

@Data
public class BannerVo {

    private String id;
    private String title;
    private String imageUrl;
    private String linkUrl;

}
