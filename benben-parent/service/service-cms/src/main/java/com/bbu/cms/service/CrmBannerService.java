package com.bbu.cms.service;

import com.bbu.cms.entity.CrmBanner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author testjava
 * @since 2022-11-20
 */
public interface CrmBannerService extends IService<CrmBanner> {

}
