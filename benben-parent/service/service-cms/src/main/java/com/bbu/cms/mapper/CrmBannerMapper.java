package com.bbu.cms.mapper;

import com.bbu.cms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
