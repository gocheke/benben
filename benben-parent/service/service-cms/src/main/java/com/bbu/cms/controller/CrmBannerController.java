package com.bbu.cms.controller;


import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bbu.cms.entity.CrmBanner;
import com.bbu.cms.entity.vo.BannerVo;
import com.bbu.cms.service.CrmBannerService;
import com.bbu.common.utils.Result;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/educms/banner")
public class CrmBannerController {

    @Autowired
    private CrmBannerService bannerService;

    @Cacheable(value = "banner", key = "'selectIndexList'")
    @GetMapping("getAllBanner")
    public Result getAllBanner(){

        List<CrmBanner> list = bannerService.list(null);

        ArrayList<BannerVo> bannerVos = new ArrayList<>();

        if (list != null && list.size() > 0){
            for (CrmBanner banner : list){
                BannerVo bannerVo = new BannerVo();
                BeanUtils.copyProperties(banner,bannerVo);
                bannerVos.add(bannerVo);
            }
        }

        return Result.ok().data("bannerList",bannerVos);
    }



}

