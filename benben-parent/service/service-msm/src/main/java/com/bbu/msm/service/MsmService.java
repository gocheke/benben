package com.bbu.msm.service;

import com.bbu.common.utils.Result;

public interface MsmService {
    Result sendMsm(String phone);
}
