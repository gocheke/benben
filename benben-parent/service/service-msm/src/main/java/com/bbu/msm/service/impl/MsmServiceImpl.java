package com.bbu.msm.service.impl;

import com.bbu.common.utils.Result;
import com.bbu.msm.service.MsmService;
import com.bbu.msm.utils.ConstantPropertiesUtil;
import com.bbu.msm.utils.MsmUtil;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MsmServiceImpl implements MsmService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Result sendMsm(String phone) {

        try {
            Result result = MsmUtil.sendCodeByShortMessage(phone, ConstantPropertiesUtil.ACCESS_KEY_ID, ConstantPropertiesUtil.ACCESS_KEY_SECRET, ConstantPropertiesUtil.HOST);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error();
        }
    }
}
