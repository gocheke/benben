package com.bbu.msm.utils;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.dysmsapi20170525.models.SendSmsResponseBody;
import com.aliyun.tea.TeaException;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.Common;
import com.aliyun.teautil.models.RuntimeOptions;
import com.bbu.common.utils.Result;

public class MsmUtil {
    /**
     * 阿里云短信api
     *
     * @param phoneNum        接受验证码的手机号
     * @param accessKeyId     用来调用第三方短信API的accessKeyId
     * @param accessKeySecret 用来调用第三方短信API的accessKeySecret
     * @param host            访问的域名
     * @return 成功返回验证码, 失败返回消息
     * @throws Exception
     */
    public static Result sendCodeByShortMessage(

            String phoneNum,

            String accessKeyId,         //LTAI5tDTTtLYMDRcFR5QheLN

            String accessKeySecret,     //izcmTzQxxHx3t4qadamsG31LAvaudq

            String host                 //dysmsapi.aliyuncs.com
    ) throws Exception {

        Client client = MsmUtil.createClient(accessKeyId, accessKeySecret, host);

        // 生成验证码
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            Integer random = (int) (Math.random() * 10);
            stringBuilder.append(random);
        }

        String code = stringBuilder.toString();

        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setSignName("阿里云短信测试")
                .setTemplateCode("SMS_154950909")
                .setPhoneNumbers(phoneNum)
                .setTemplateParam("{\"code\":\"" + code + "\"}");
        RuntimeOptions runtime = new RuntimeOptions();

        try {
            // 复制代码运行请自行打印 API 的返回值
            SendSmsResponse sendSmsResponse = client.sendSmsWithOptions(sendSmsRequest, runtime);
            SendSmsResponseBody body = sendSmsResponse.getBody(); //message:OK(只能向已回复授权信息的手机号发送)  code:OK

            // 状态码
            Integer statusCode = sendSmsResponse.getStatusCode();
            // 返回的信息
            String message = body.getMessage();

            if (statusCode == 200 && "OK".equals(message)) {
                // 操作成功,把生成的验证码返回
                return Result.ok().data("code",code);
            }

            return Result.error().message(message);

        } catch (TeaException error) {
            // 如有需要，请打印 error
            Common.assertAsString(error.message);
            return Result.error().message(error.message);
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 如有需要，请打印 error
            Common.assertAsString(error.message);
            return Result.error().message(error.message);
        }

    }

    /**
     * 阿里云短信验证accessKeyId与accessKeySecret方法
     *
     * @param accessKeyId
     * @param accessKeySecret
     * @return
     * @throws Exception
     */
    public static Client createClient(String accessKeyId, String accessKeySecret, String host) throws Exception {

        Config config = new Config()
                // 您的 AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的 AccessKey Secret
                .setAccessKeySecret(accessKeySecret);

        // 访问的域名
        config.endpoint = host;
        return new Client(config);
    }
}
