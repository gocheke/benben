package com.bbu.msm.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.bbu.common.utils.Result;
import com.bbu.common.utils.ResultCode;
import com.bbu.msm.service.MsmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/edumsm/msm")
public class MsmController {

    @Autowired
    private MsmService msmService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @GetMapping(value = "/send/{phone}")
    public Result code(@PathVariable String phone) {

        String code = redisTemplate.opsForValue().get(phone);

        if(!StringUtils.isEmpty(code)){
            return Result.ok();
        }

        Result result = msmService.sendMsm(phone);

        if (result.getCode() == ResultCode.SUCCESS) {
            Map data = result.getData();
            String codeStr = (String) data.get("code");

            redisTemplate.opsForValue().set(phone,codeStr,5,TimeUnit.MINUTES);

            return Result.ok();
        }else {
            return Result.error().message("短信发送失败");
        }

    }

}
