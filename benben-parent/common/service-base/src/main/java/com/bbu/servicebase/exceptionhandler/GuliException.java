package com.bbu.servicebase.exceptionhandler;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GuliException extends RuntimeException{

    private static final long serialVersionUID = -2445998815781254282L;

    @ApiModelProperty(value = "状态码")
    private Integer code;

    private String msg;
}
