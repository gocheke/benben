/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.25-log : Database - benben
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`benben` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `benben`;

/*Table structure for table `acl_permission` */

DROP TABLE IF EXISTS `acl_permission`;

CREATE TABLE `acl_permission` (
  `id` char(19) NOT NULL DEFAULT '' COMMENT '编号',
  `pid` char(19) NOT NULL DEFAULT '' COMMENT '所属上级',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '名称',
  `type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '类型(1:菜单,2:按钮)',
  `permission_value` varchar(50) DEFAULT NULL COMMENT '权限值',
  `path` varchar(100) DEFAULT NULL COMMENT '访问路径',
  `component` varchar(100) DEFAULT NULL COMMENT '组件路径',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态(0:禁止,1:正常)',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_pid` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='权限';

/*Data for the table `acl_permission` */

insert  into `acl_permission`(`id`,`pid`,`name`,`type`,`permission_value`,`path`,`component`,`icon`,`status`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1','0','全部数据',0,'123','123',NULL,NULL,NULL,0,'2019-11-15 17:13:06','2019-11-15 17:13:06'),('1195268474480156673','1','权限管理',1,NULL,'/acl','Layout',NULL,NULL,0,'2019-11-15 17:13:06','2019-11-18 13:54:25'),('1195268616021139457','1195268474480156673','用户管理',1,NULL,'user/list','/acl/user/list',NULL,NULL,0,'2019-11-15 17:13:40','2019-11-18 13:53:12'),('1195268788138598401','1195268474480156673','角色管理',1,NULL,'role/list','/acl/role/list',NULL,NULL,0,'2019-11-15 17:14:21','2019-11-15 17:14:21'),('1195268893830864898','1195268474480156673','菜单管理',1,NULL,'menu/list','/acl/menu/list',NULL,NULL,0,'2019-11-15 17:14:46','2019-11-15 17:14:46'),('1195269143060602882','1195268616021139457','查看',2,'user.list','','',NULL,NULL,0,'2019-11-15 17:15:45','2019-11-17 21:57:16'),('1195269295926206466','1195268616021139457','添加',2,'user.add','user/add','/acl/user/form',NULL,NULL,0,'2019-11-15 17:16:22','2019-11-15 17:16:22'),('1195269473479483394','1195268616021139457','修改',2,'user.update','user/update/:id','/acl/user/form',NULL,NULL,0,'2019-11-15 17:17:04','2019-11-15 17:17:04'),('1195269547269873666','1195268616021139457','删除',2,'user.remove','','',NULL,NULL,0,'2019-11-15 17:17:22','2019-11-15 17:17:22'),('1195269821262782465','1195268788138598401','修改',2,'role.update','role/update/:id','/acl/role/form',NULL,NULL,0,'2019-11-15 17:18:27','2019-11-15 17:19:53'),('1195269903542444034','1195268788138598401','查看',2,'role.list','','',NULL,NULL,0,'2019-11-15 17:18:47','2019-11-15 17:18:47'),('1195270037005197313','1195268788138598401','添加',2,'role.add','role/add','/acl/role/form',NULL,NULL,0,'2019-11-15 17:19:19','2019-11-18 11:05:42'),('1195270442602782721','1195268788138598401','删除',2,'role.remove','','',NULL,NULL,0,'2019-11-15 17:20:55','2019-11-15 17:20:55'),('1195270621548568578','1195268788138598401','角色权限',2,'role.acl','role/distribution/:id','/acl/role/roleForm',NULL,NULL,0,'2019-11-15 17:21:38','2019-11-15 17:21:38'),('1195270744097742849','1195268893830864898','查看',2,'permission.list','','',NULL,NULL,0,'2019-11-15 17:22:07','2019-11-15 17:22:07'),('1195270810560684034','1195268893830864898','添加',2,'permission.add','','',NULL,NULL,0,'2019-11-15 17:22:23','2019-11-15 17:22:23'),('1195270862100291586','1195268893830864898','修改',2,'permission.update','','',NULL,NULL,0,'2019-11-15 17:22:35','2019-11-15 17:22:35'),('1195270887933009922','1195268893830864898','删除',2,'permission.remove','','',NULL,NULL,0,'2019-11-15 17:22:41','2019-11-15 17:22:41'),('1195349439240048642','1','讲师管理',1,NULL,'/teacher','Layout',NULL,NULL,0,'2019-11-15 22:34:49','2019-11-15 22:34:49'),('1195349699995734017','1195349439240048642','讲师列表',1,NULL,'table','/edu/teacher/list',NULL,NULL,0,'2019-11-15 22:35:52','2019-11-15 22:35:52'),('1195349810561781761','1195349439240048642','添加讲师',1,NULL,'save','/edu/teacher/save',NULL,NULL,0,'2019-11-15 22:36:18','2019-11-15 22:36:18'),('1195349876252971010','1195349810561781761','添加',2,'teacher.add','','',NULL,NULL,0,'2019-11-15 22:36:34','2019-11-15 22:36:34'),('1195349979797753857','1195349699995734017','查看',2,'teacher.list','','',NULL,NULL,0,'2019-11-15 22:36:58','2019-11-15 22:36:58'),('1195350117270261762','1195349699995734017','修改',2,'teacher.update','edit/:id','/edu/teacher/save',NULL,NULL,0,'2019-11-15 22:37:31','2019-11-15 22:37:31'),('1195350188359520258','1195349699995734017','删除',2,'teacher.remove','','',NULL,NULL,0,'2019-11-15 22:37:48','2019-11-15 22:37:48'),('1195350299365969922','1','课程分类',1,NULL,'/subject','Layout',NULL,NULL,0,'2019-11-15 22:38:15','2019-11-15 22:38:15'),('1195350397751758850','1195350299365969922','课程分类列表',1,NULL,'list','/edu/subject/list',NULL,NULL,0,'2019-11-15 22:38:38','2019-11-15 22:38:38'),('1195350500512206850','1195350299365969922','导入课程分类',1,NULL,'save','/edu/subject/save',NULL,NULL,0,'2019-11-15 22:39:03','2019-11-15 22:39:03'),('1195350612172967938','1195350397751758850','查看',2,'subject.list','','',NULL,NULL,0,'2019-11-15 22:39:29','2019-11-15 22:39:29'),('1195350687590748161','1195350500512206850','导入',2,'subject.import','','',NULL,NULL,0,'2019-11-15 22:39:47','2019-11-15 22:39:47'),('1195350831744782337','1','课程管理',1,NULL,'/course','Layout',NULL,NULL,0,'2019-11-15 22:40:21','2019-11-15 22:40:21'),('1195350919074385921','1195350831744782337','课程列表',1,NULL,'list','/edu/course/list',NULL,NULL,0,'2019-11-15 22:40:42','2019-11-15 22:40:42'),('1195351020463296513','1195350831744782337','发布课程',1,NULL,'info','/edu/course/info',NULL,NULL,0,'2019-11-15 22:41:06','2019-11-15 22:41:06'),('1195351159672246274','1195350919074385921','完成发布',2,'course.publish','publish/:id','/edu/course/publish',NULL,NULL,0,'2019-11-15 22:41:40','2019-11-15 22:44:01'),('1195351326706208770','1195350919074385921','编辑课程',2,'course.update','info/:id','/edu/course/info',NULL,NULL,0,'2019-11-15 22:42:19','2019-11-15 22:42:19'),('1195351566221938690','1195350919074385921','编辑课程大纲',2,'chapter.update','chapter/:id','/edu/course/chapter',NULL,NULL,0,'2019-11-15 22:43:17','2019-11-15 22:43:17'),('1195351862889254913','1','统计分析',1,NULL,'/sta','Layout',NULL,NULL,0,'2019-11-15 22:44:27','2019-11-15 22:44:27'),('1195351968841568257','1195351862889254913','生成统计',1,NULL,'create','/sta/create',NULL,NULL,0,'2019-11-15 22:44:53','2019-11-15 22:44:53'),('1195352054917074946','1195351862889254913','统计图表',1,NULL,'show','/sta/show',NULL,NULL,0,'2019-11-15 22:45:13','2019-11-15 22:45:13'),('1195352127734386690','1195352054917074946','查看',2,'daily.list','','',NULL,NULL,0,'2019-11-15 22:45:30','2019-11-15 22:45:30'),('1195352215768633346','1195351968841568257','生成',2,'daily.add','','',NULL,NULL,0,'2019-11-15 22:45:51','2019-11-15 22:45:51'),('1195352547621965825','1','CMS管理',1,NULL,'/cms','Layout',NULL,NULL,0,'2019-11-15 22:47:11','2019-11-18 10:51:46'),('1195352856645701633','1195353513549205505','查看',2,'banner.list','',NULL,NULL,NULL,0,'2019-11-15 22:48:24','2019-11-15 22:48:24'),('1195352909401657346','1195353513549205505','添加',2,'banner.add','banner/add','/edu/banner/save',NULL,NULL,0,'2019-11-15 22:48:37','2019-11-18 10:52:10'),('1195353051395624961','1195353513549205505','修改',2,'banner.update','banner/update/:id','/edu/banner/save',NULL,NULL,0,'2019-11-15 22:49:11','2019-11-18 10:52:05'),('1195353513549205505','1195352547621965825','Bander列表',1,NULL,'banner/list','/edu/banner/list',NULL,NULL,0,'2019-11-15 22:51:01','2019-11-18 10:51:29'),('1195353672110673921','1195353513549205505','删除',2,'banner.remove','','',NULL,NULL,0,'2019-11-15 22:51:39','2019-11-15 22:51:39'),('1195354076890370050','1','订单管理',1,NULL,'/order','Layout',NULL,NULL,0,'2019-11-15 22:53:15','2019-11-15 22:53:15'),('1195354153482555393','1195354076890370050','订单列表',1,NULL,'list','/order/list',NULL,NULL,0,'2019-11-15 22:53:33','2019-11-15 22:53:58'),('1195354315093282817','1195354153482555393','查看',2,'order.list','','',NULL,NULL,0,'2019-11-15 22:54:12','2019-11-15 22:54:12'),('1196301740985311234','1195268616021139457','分配角色',2,'user.assgin','user/role/:id','/acl/user/roleForm',NULL,NULL,0,'2019-11-18 13:38:56','2019-11-18 13:38:56');

/*Table structure for table `acl_role` */

DROP TABLE IF EXISTS `acl_role`;

CREATE TABLE `acl_role` (
  `id` char(19) NOT NULL DEFAULT '' COMMENT '角色id',
  `role_name` varchar(20) NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_code` varchar(20) DEFAULT NULL COMMENT '角色编码',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acl_role` */

insert  into `acl_role`(`id`,`role_name`,`role_code`,`remark`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1','普通管理员',NULL,NULL,0,'2019-11-11 13:09:32','2019-11-18 10:27:18'),('1193757683205607426','课程管理员',NULL,NULL,0,'2019-11-11 13:09:45','2019-11-18 10:25:44'),('1196300996034977794','test',NULL,NULL,0,'2019-11-18 13:35:58','2019-11-18 13:35:58'),('1606205182771740673','测试001',NULL,NULL,0,'2022-12-23 16:28:52','2022-12-23 16:28:52');

/*Table structure for table `acl_role_permission` */

DROP TABLE IF EXISTS `acl_role_permission`;

CREATE TABLE `acl_role_permission` (
  `id` char(19) NOT NULL DEFAULT '',
  `role_id` char(19) NOT NULL DEFAULT '',
  `permission_id` char(19) NOT NULL DEFAULT '',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限';

/*Data for the table `acl_role_permission` */

insert  into `acl_role_permission`(`id`,`role_id`,`permission_id`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1606205265248534529','1196300996034977794','1195268616021139457',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265265311746','1196300996034977794','1195269143060602882',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265265311747','1196300996034977794','1195269295926206466',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265265311748','1196300996034977794','1195269473479483394',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265265311749','1196300996034977794','1195269547269873666',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894657','1196300996034977794','1196301740985311234',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894658','1196300996034977794','1195268788138598401',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894659','1196300996034977794','1195269821262782465',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894660','1196300996034977794','1195269903542444034',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894661','1196300996034977794','1195270037005197313',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894662','1196300996034977794','1195270442602782721',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894663','1196300996034977794','1195270621548568578',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894664','1196300996034977794','1195349439240048642',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894665','1196300996034977794','1195349699995734017',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894666','1196300996034977794','1195349979797753857',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265277894667','1196300996034977794','1195350117270261762',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265294671873','1196300996034977794','1195350188359520258',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265294671874','1196300996034977794','1195349810561781761',0,'2022-12-23 16:29:12','2022-12-23 16:29:12'),('1606205265294671875','1196300996034977794','1195349876252971010',0,'2022-12-23 16:29:12','2022-12-23 16:29:12');

/*Table structure for table `acl_user` */

DROP TABLE IF EXISTS `acl_user`;

CREATE TABLE `acl_user` (
  `id` char(19) NOT NULL COMMENT '会员id',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '微信openid',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `salt` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `token` varchar(100) DEFAULT NULL COMMENT '用户签名',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

/*Data for the table `acl_user` */

insert  into `acl_user`(`id`,`username`,`password`,`nick_name`,`salt`,`token`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1','admin','96e79218965eb72c92a549dd5a330112','admin','',NULL,0,'2019-11-01 10:39:47','2019-11-01 10:39:47'),('2','test','96e79218965eb72c92a549dd5a330112','test',NULL,NULL,0,'2019-11-01 16:36:07','2019-11-01 16:40:08');

/*Table structure for table `acl_user_role` */

DROP TABLE IF EXISTS `acl_user_role`;

CREATE TABLE `acl_user_role` (
  `id` char(19) NOT NULL DEFAULT '' COMMENT '主键id',
  `role_id` char(19) NOT NULL DEFAULT '0' COMMENT '角色id',
  `user_id` char(19) NOT NULL DEFAULT '0' COMMENT '用户id',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acl_user_role` */

insert  into `acl_user_role`(`id`,`role_id`,`user_id`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1606205525232467969','1606205182771740673','2',0,'2022-12-23 16:30:14','2022-12-23 16:30:14');

/*Table structure for table `crm_banner` */

DROP TABLE IF EXISTS `crm_banner`;

CREATE TABLE `crm_banner` (
  `id` char(19) NOT NULL DEFAULT '' COMMENT 'ID',
  `title` varchar(20) DEFAULT '' COMMENT '标题',
  `image_url` varchar(500) NOT NULL DEFAULT '' COMMENT '图片地址',
  `link_url` varchar(500) DEFAULT '' COMMENT '链接地址',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='首页banner表';

/*Data for the table `crm_banner` */

insert  into `crm_banner`(`id`,`title`,`image_url`,`link_url`,`sort`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('119460745846','test2','http://guli.shop/photo/course/02.jpg','/teacher',2,0,'2019-11-13 21:26:27','2019-11-14 09:12:15'),('1594309044800106497','888111','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/20/c99c220f3e3942fd851ebc9445dd6260.jpg','/teacher',0,0,'2022-11-20 20:37:52','2022-11-20 20:51:21'),('1594312730880290817','是德国','http://guli.shop/photo/course/02.jpg','/',0,1,'2022-11-20 20:52:31','2022-11-20 20:52:31'),('1594312759510609922','f的 ','http://guli.shop/photo/course/02.jpg','/',0,1,'2022-11-20 20:52:37','2022-11-20 20:52:37'),('1594312781790752769','广泛的是','http://guli.shop/photo/course/02.jpg','/',0,1,'2022-11-20 20:52:43','2022-11-20 20:52:43'),('1594312808105816066','发广告','http://guli.shop/photo/course/02.jpg','/',0,1,'2022-11-20 20:52:49','2022-11-20 20:52:49'),('1594313203855175681','后端的非常差','http://guli.shop/photo/course/02.jpg','/',0,1,'2022-11-20 20:54:23','2022-11-20 20:54:23'),('1594313230027632641','就会返回','http://guli.shop/photo/course/02.jpg','/',0,1,'2022-11-20 20:54:30','2022-11-20 20:54:30'),('1594313253394100225','灰姑娘','http://guli.shop/photo/course/02.jpg','/',0,1,'2022-11-20 20:54:35','2022-11-20 20:54:35'),('1594317450302763010','琪琪','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/20/9b6a39224f2a48e298c71e0fdc561321.jpg','/course',0,0,'2022-11-20 21:11:16','2022-11-20 21:11:16');

/*Table structure for table `edu_chapter` */

DROP TABLE IF EXISTS `edu_chapter`;

CREATE TABLE `edu_chapter` (
  `id` char(19) NOT NULL COMMENT '章节ID',
  `course_id` char(19) NOT NULL COMMENT '课程ID',
  `title` varchar(50) NOT NULL COMMENT '章节名称',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '显示排序',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程';

/*Data for the table `edu_chapter` */

insert  into `edu_chapter`(`id`,`course_id`,`title`,`sort`,`gmt_create`,`gmt_modified`) values ('1592159418819141634','1592159323008655362','第一章：如何发现电子小鬼？',0,'2022-11-14 22:16:01','2022-11-14 22:16:22'),('1593129529566568449','18','第一章',0,'2022-11-17 14:30:53','2022-11-17 14:30:53'),('1596732703833612290','18','第二章',0,'2022-11-27 13:08:37','2022-11-27 13:08:37'),('1597164473548820481','1594320414681595906','第一章',0,'2022-11-28 17:44:19','2022-11-28 17:44:19');

/*Table structure for table `edu_comment` */

DROP TABLE IF EXISTS `edu_comment`;

CREATE TABLE `edu_comment` (
  `id` char(19) NOT NULL COMMENT 'ID',
  `course_id` varchar(19) NOT NULL DEFAULT '' COMMENT '课程id',
  `teacher_id` char(19) NOT NULL DEFAULT '' COMMENT '讲师id',
  `member_id` varchar(19) NOT NULL DEFAULT '' COMMENT '会员id',
  `nickname` varchar(50) DEFAULT NULL COMMENT '会员昵称',
  `avatar` varchar(255) DEFAULT NULL COMMENT '会员头像',
  `content` varchar(500) DEFAULT NULL COMMENT '评论内容',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_teacher_id` (`teacher_id`),
  KEY `idx_member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='评论';

/*Data for the table `edu_comment` */

insert  into `edu_comment`(`id`,`course_id`,`teacher_id`,`member_id`,`nickname`,`avatar`,`content`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1194499162790211585','18','1189389726308478977','1','小三123','http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132','课程很好',0,'2019-11-13 14:16:08','2019-11-13 14:16:08'),('1194898406466420738','18','1189389726308478977','1','小三123','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/default.jpg','11',0,'2019-11-14 16:42:35','2019-11-14 16:42:35'),('1194898484388200450','18','1189389726308478977','1','小三123','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/default.jpg','111',0,'2019-11-14 16:42:53','2019-11-14 16:42:53'),('1195251020861317122','18','1189389726308478977','1',NULL,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/default.jpg','2233',0,'2019-11-15 16:03:45','2019-11-15 16:03:45'),('1195251382720700418','18','1189389726308478977','1',NULL,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/default.jpg','4455',0,'2019-11-15 16:05:11','2019-11-15 16:05:11'),('1195252819177570306','18','1189389726308478977','1','小三1231','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/default.jpg','55',0,'2019-11-15 16:10:53','2019-11-15 16:10:53'),('1195252899448160258','18','1189389726308478977','1','小三1231','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/default.jpg','55',0,'2019-11-15 16:11:13','2019-11-15 16:11:13'),('1195252920587452417','18','1189389726308478977','1','小三1231','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/default.jpg','223344',0,'2019-11-15 16:11:18','2019-11-15 16:11:18'),('1195262128095559681','14','1189389726308478977','1','小三1231','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/default.jpg','11',0,'2019-11-15 16:47:53','2019-11-15 16:47:53'),('1196264505170767874','18','1189389726308478977','1','小三1231','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/default.jpg','666666',0,'2019-11-18 11:10:58','2019-11-18 11:10:58'),('1597502727984885761','18','1587433401059639298','1597499492427403266','~','https://thirdwx.qlogo.cn/mmopen/vi_32/DiasSKoTz6nPFWHrsClsnBjklibLCdnLlNnMoLclLDzjVo3DwzQDpg4iba98ZJib1wwFuMNMwUSiaOwFAHuv9SvngBw/132','你好',0,'2022-11-29 16:08:25','2022-11-29 16:08:25'),('1597502823208169473','18','1587433401059639298','1597499492427403266','~','https://thirdwx.qlogo.cn/mmopen/vi_32/DiasSKoTz6nPFWHrsClsnBjklibLCdnLlNnMoLclLDzjVo3DwzQDpg4iba98ZJib1wwFuMNMwUSiaOwFAHuv9SvngBw/132','我爱你',0,'2022-11-29 16:08:48','2022-11-29 16:08:48'),('1597503241686462465','18','1587433401059639298','1597499492427403266','~','https://thirdwx.qlogo.cn/mmopen/vi_32/DiasSKoTz6nPFWHrsClsnBjklibLCdnLlNnMoLclLDzjVo3DwzQDpg4iba98ZJib1wwFuMNMwUSiaOwFAHuv9SvngBw/132','哈哈哈',0,'2022-11-29 16:10:28','2022-11-29 16:10:28'),('1597503304923983873','18','1587433401059639298','1597499492427403266','~','https://thirdwx.qlogo.cn/mmopen/vi_32/DiasSKoTz6nPFWHrsClsnBjklibLCdnLlNnMoLclLDzjVo3DwzQDpg4iba98ZJib1wwFuMNMwUSiaOwFAHuv9SvngBw/132','我喜欢这门课',0,'2022-11-29 16:10:43','2022-11-29 16:10:43'),('1597503846949654530','18','1587433401059639298','1597499492427403266','~','https://thirdwx.qlogo.cn/mmopen/vi_32/DiasSKoTz6nPFWHrsClsnBjklibLCdnLlNnMoLclLDzjVo3DwzQDpg4iba98ZJib1wwFuMNMwUSiaOwFAHuv9SvngBw/132','我是小哼哼',0,'2022-11-29 16:12:52','2022-11-29 16:12:52'),('1597503982039797761','1594320414681595906','1587433401059639298','1594999162565296129','che','http://aliyun_id_photo_bucket.oss.aliyuncs.com/default_handsome.jpg','这是琪琪的项目',0,'2022-11-29 16:13:24','2022-11-29 16:13:24'),('1597504030043607041','1594320414681595906','1587433401059639298','1594999162565296129','che','http://aliyun_id_photo_bucket.oss.aliyuncs.com/default_handsome.jpg','666',0,'2022-11-29 16:13:36','2022-11-29 16:13:36'),('1597504361452343298','1594320414681595906','1587433401059639298','1594999162565296129','che','http://aliyun_id_photo_bucket.oss.aliyuncs.com/default_handsome.jpg','大家不要学，这老师教的不行',0,'2022-11-29 16:14:55','2022-11-29 16:14:55'),('1597506422680129537','18','1587433401059639298','1594999162565296129','che','http://aliyun_id_photo_bucket.oss.aliyuncs.com/default_handsome.jpg','我是ck',0,'2022-11-29 16:23:06','2022-11-29 16:23:06'),('1597856317769322497','1592159323008655362','1587433401059639298','1594999162565296129','che','http://aliyun_id_photo_bucket.oss.aliyuncs.com/default_handsome.jpg','你好啊',0,'2022-11-30 15:33:28','2022-11-30 15:33:28');

/*Table structure for table `edu_course` */

DROP TABLE IF EXISTS `edu_course`;

CREATE TABLE `edu_course` (
  `id` char(19) NOT NULL COMMENT '课程ID',
  `teacher_id` char(19) DEFAULT NULL COMMENT '课程讲师ID',
  `subject_id` char(19) DEFAULT NULL COMMENT '课程专业ID',
  `subject_parent_id` char(19) DEFAULT NULL COMMENT '课程专业父级ID',
  `title` varchar(50) DEFAULT NULL COMMENT '课程标题',
  `price` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '课程销售价格，设置为0则可免费观看',
  `lesson_num` int(10) unsigned DEFAULT '0' COMMENT '总课时',
  `cover` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '课程封面图片路径',
  `buy_count` bigint(10) unsigned DEFAULT '0' COMMENT '销售数量',
  `view_count` bigint(10) unsigned DEFAULT '0' COMMENT '浏览数量',
  `version` bigint(20) unsigned DEFAULT '1' COMMENT '乐观锁',
  `status` varchar(10) DEFAULT 'Draft' COMMENT '课程状态 Draft未发布  Normal已发布',
  `is_deleted` tinyint(3) DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_title` (`title`),
  KEY `idx_subject_id` (`subject_id`),
  KEY `idx_teacher_id` (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程';

/*Data for the table `edu_course` */

insert  into `edu_course`(`id`,`teacher_id`,`subject_id`,`subject_parent_id`,`title`,`price`,`lesson_num`,`cover`,`buy_count`,`view_count`,`version`,`status`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1592159323008655362','1587433401059639298','1589896160959565825','1589896160896651265','心理学入门','0.00',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/defaultCourse.jpg',0,3,1,'Normal',0,'2022-11-14 22:15:38','2022-11-28 12:28:25'),('1593602718482579457','1','1589896160829542401','1589896160762433538','最终测试绝对没bug','1.00',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/defaultCourse.jpg',0,4,1,'Normal',1,'2022-11-18 21:51:10','2022-11-18 21:52:00'),('1594320414681595906','1587433401059639298','1589896160959565825','1589896160896651265','琪琪1','1.00',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/defaultCourse.jpg',65,5,1,'Normal',0,'2022-11-20 21:23:03','2022-11-26 16:27:02'),('1594320728109350914','1592534305597325313','1590922444481282051','1590922444481282050','6666','0.01',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/a7d4252f696545d5a78236c9bb875209.jpg',0,6,1,'Normal',0,'2022-11-20 21:24:17','2022-11-21 11:10:50'),('1594321225629302785','1587433401059639298','1589896160959565825','1589896160896651265','你好777','0.01',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/defaultCourse.jpg',65,7,1,'Normal',0,'2022-11-20 21:26:16','2022-11-26 16:47:15'),('1594321309020454914','1189426437876985857','1589896160959565825','1589896160896651265','看是否','0.01',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/defaultCourse.jpg',0,8,1,'Normal',0,'2022-11-20 21:26:36','2022-11-20 21:26:39'),('1594322011138555906','1587433401059639298','1589896160959565825','1589896160896651265','kfjhffg','0.01',0,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/defaultCourse.jpg',34,9,1,'Normal',1,'2022-11-20 21:29:23','2022-11-26 13:39:39'),('1594322558847549441','1587433401059639298','1590922444481282051','1590922444481282050','的分公司的','0.01',4,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/b09af08ee78e427dad6c6df0d2bde738.jpg',53,10,1,'Normal',0,'2022-11-20 21:31:34','2022-11-26 16:47:43'),('1596480241566482434','1189426437876985857','1589896160959565825','1589896160896651265','发v表现出','0.01',0,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/defaultCourse.jpg',66,0,1,'Normal',0,'2022-11-26 20:25:25','2022-11-26 20:25:29'),('1605906254419505153','','','','','0.00',0,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/defaultCourse.jpg',0,0,1,'Normal',1,'2022-12-22 20:41:02','2022-12-22 20:41:08'),('18','1587433401059639298','1590922444481282051','1590922444481282050','18无敌','0.01',0,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/8d8207a076e84c239fc11d57b93fdf7c.jpg',151,737,6,'Normal',0,'2018-04-02 21:28:46','2022-11-27 13:14:38');

/*Table structure for table `edu_course_collect` */

DROP TABLE IF EXISTS `edu_course_collect`;

CREATE TABLE `edu_course_collect` (
  `id` char(19) NOT NULL COMMENT '收藏ID',
  `course_id` char(19) NOT NULL COMMENT '课程讲师ID',
  `member_id` char(19) NOT NULL DEFAULT '' COMMENT '课程专业ID',
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程收藏';

/*Data for the table `edu_course_collect` */

insert  into `edu_course_collect`(`id`,`course_id`,`member_id`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1196269345666019330','1192252213659774977','1',1,'2019-11-18 11:30:12','2019-11-18 11:30:12');

/*Table structure for table `edu_course_description` */

DROP TABLE IF EXISTS `edu_course_description`;

CREATE TABLE `edu_course_description` (
  `id` char(19) NOT NULL COMMENT '课程ID',
  `description` text COMMENT '课程简介',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='课程简介';

/*Data for the table `edu_course_description` */

insert  into `edu_course_description`(`id`,`description`,`gmt_create`,`gmt_modified`) values ('1104870479077879809','<p>11</p>','2019-03-11 06:23:44','2019-03-11 06:23:44'),('1192252213659774977','<p>测试</p>','2019-11-07 09:27:33','2019-11-13 16:21:28'),('14','','2019-03-13 06:04:43','2019-03-13 06:05:33'),('15','','2019-03-13 06:03:33','2019-03-13 06:04:22'),('1591351802471673857','string','2022-11-12 16:46:50','2022-11-12 16:46:50'),('1591351969648242689','string','2022-11-12 16:47:30','2022-11-12 16:47:30'),('1591404306605539330','啊啊啊','2022-11-12 20:15:28','2022-11-12 20:15:28'),('1591407701370347522','啊啊啊','2022-11-12 20:28:58','2022-11-12 20:28:58'),('1591410338106974209','发起官方','2022-11-12 20:39:26','2022-11-12 20:39:26'),('1591410461998325761','威锋网','2022-11-12 20:39:56','2022-11-12 20:39:56'),('1591410964652105729','二十天','2022-11-12 20:41:56','2022-11-12 20:41:56'),('1591411075792773122','合法的','2022-11-12 20:42:22','2022-11-12 20:42:22'),('1591411441594916865','年后','2022-11-12 20:43:49','2022-11-12 20:43:49'),('1591421821780090881','个首付大概','2022-11-12 21:25:04','2022-11-12 21:25:04'),('1591445454812475393','666','2022-11-12 22:58:59','2022-11-12 22:58:59'),('1591446218683191298','方法','2022-11-12 23:02:01','2022-11-12 23:02:01'),('1591446597399482369','公共航空','2022-11-12 23:03:31','2022-11-12 23:03:31'),('1591457580784291841','等你答复12','2022-11-12 23:47:10','2022-11-14 10:30:29'),('1591458126219972609','m\'】哦了','2022-11-12 23:49:20','2022-11-12 23:49:20'),('1591660214971535361','<h3>三级标题</h3>\n<p><img class=\"wscnph\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wgARCAB4AHgDASIAAhEBAxEB/8QAGgABAAIDAQAAAAAAAAAAAAAAAAMEAgUGAf/EABgBAQADAQAAAAAAAAAAAAAAAAABAgQD/9oADAMBAAIQAxAAAAHfDpxAAKtKLbdrthMehAAAAHnMWdbz70HVx8u/MWt/URuLXFdp3y+i1AAAOK7zjuwyb5nnvO4hOV2df3Tm3A7ZgABlE89vYZcm/K7WsVtXg2GoRUx2uPbjKNGQABJGiZLEE+PeilwrbMEWElfvnDRlAAA9tVMufWeCxll2wyq9qMDXhCYAAAFKYuIrWbbF7hQ7ZtkwztQAAACrSL1CYAz2ZWfRWwH/xAAnEAACAQIGAQQDAQAAAAAAAAABAgMABAUQERIgMDEUFSE0EyMyQP/aAAgBAQABBQL/ACyXMUVe4w0t7A1AgjpJ0Fzes5zineEwTrOnRiE9W9pJcUMKSjhPz7StS4ZIohka3mB1HNtZ7qNBHHwxNNtzZNuteeHLuvDLGpzMsYOLCsO+vzwoft9HbmktkiapIhLXorcViSBLbDvr8gNTa2yW73azNBAHWDK1S5WW6hE8UUCQR8k8oCDk2b/z4TmvnLQa5N46R80wJGx6UEZMdT0g6UDrmW17d7dry1bMDX4JKuNFhWRlpWDDolbQcVbafIz/AP/EACARAAEEAwEAAwEAAAAAAAAAAAEAAgMREBIgISIwMUH/2gAIAQMBAT8BzqUQR1Gz+rUotT26nlgzN+cAWaQHmZh5w00bTHbC8zSV8eYpNUDafIGomzZ6geNfVK63n6Rn/8QAHxEAAgEFAQADAAAAAAAAAAAAAQIAAxAREiAhIjFC/9oACAECAQE/Ab5Ez0zTYQMIpzy96XBOIT7ek3BGRiMupxekn65qJtPqKhaAY86qqdpTX49C5v8A/8QALRAAAQMBBQYFBQAAAAAAAAAAAQACESEDIDAxURASIkBhkTIzQXFyE1KBgqH/2gAIAQEABj8C5XieJ0WT+y8Ue6kYUlbtnRuutyWn8KRn6jB+iP2XDRupVbRy4bXuF5p7KWOD1PcKRgGM3OQY3IXQ77gm9KYAOglQ57QepuQXtnSVZH3TvlgWh6KtmCpsy5vSabOJzo0BXlBWYbkHJ3ywH7jiZ1UWJh0potTL/Xa82zpb6VQaTFZUMmOuAdo97gHM0K8SqedzVcSGq0a50F2RKy/qDJBdvTTZTB3b0qbn/8QAJxABAAIABAUFAAMAAAAAAAAAAQARITAxQRAgUWGBcZGhscFA0fD/2gAIAQEAAT8h/ipJ2hixB1PD+47Vn21LECO5lAyUGrFTvvuRpd7rRmh8eDJQpe/4InYAcdKHMc9ioXQxKN6V1M2qmGapTUgJLEsciubEQdKOjlIA6vrHuatyHfq/P7O07wGDZZxo99Y3MD0X1PnfoyLulJ8xQqjqqrK4PpuvDww0e+sMDV7qsOiqwLezPnfo58IiSUTDZESqWjSnrK5AYpYb8L/Z3Me3SPzIbBcAuk2uq+fB4y9jj9fiVwESw1yGuJ43Wr54utU1yBpuIFzQAz/Jm7fbh6BlK4Aw4KBjPEzGws24AVxWYtuh1gYgCx0nSCdaTduS11hN0s6MvXsycMauvM1bzBKGjyf/2gAMAwEAAgADAAAAEPvvtsPvvvvtMI8/vvvvSw1fvvvjSM3fvvralw5fvvvqYNv/AL77735fb7776AAB/wC+/8QAGxEBAAIDAQEAAAAAAAAAAAAAAQARICExEFH/2gAIAQMBAT8Q9F2E6Blft3wVNk5nMQD04EAdZRtCNXyKtMKf4g1PaSTGzTxgCyE77Hbo4rU1ahIOZPcn/8QAHREBAAIDAQADAAAAAAAAAAAAAQARECAhMUFRYf/aAAgBAgEBPxDKXzAPm1fMVrjKWqVrFRXWgC2WcMXsFrjAEH3S6+0axm2ra0rPYiqYx+QjTUFnYCIHYcyc/wD/xAAnEAEAAQMCBAcBAQAAAAAAAAABEQAhQTFREDBxkSBhgaGxwdHwQP/aAAgBAQABPxD/AChFDyR6ExUUCbkHvQ4msQu9yjA2kSR5QQTKjAFApciLfke++1TLLrxPqmzOfS/msY4Kyv8AOSxFiDDr/E9qdQGWQdjd/lKEm6zHvNMCw25KnZ/KsKu4AfNC2RM5fTUe5SglDxFMib/dFcGBojyJGTJ28CwdiKgniWXz9fnwmOAVGQp8RRopkPRt7JyBUJGp6fYUQSNGj0WgBBEkRkTiJamABPSaNMaKvcfdJ6HIs4C4j1H5SASVYPVZo0Msruok9ofPhDE5HqNi/vHlUVMbod1phMhCApEsuPTkQzmMAT6UJFiA0LkiddqlRkcsSFkY2qSJpNM3Yvm0Xy0wJBLBLE8ElGgMTLUYRNrYtV0PYRQT7qX6gJSzjbxouMSg62oAcIgY14uJ/wAX4wlFksEtJbEljvyHIja0s54pDSjJKgenFxhCtkYaVUqru8hzGo0IDI1BGLOGpX96fVTHmpQJbBmtG0WOVMC46m9T5XyZOE6UFLaW2ZeY7jFDJircfApdYqxLy5AlsGaRYRZ1rSFiYIlLE+clSX2ag+aU7XDGRE9qYBedJqVEJqtTkuC2JRg8Qm4FhuUYWQEfB//Z\" /></p>\n<p>![img](D:\\\\defaultCourse.jpg)</p>','2022-11-13 13:12:22','2022-11-13 13:12:22'),('1591773889795534850','<p>烦烦烦</p>','2022-11-13 20:44:04','2022-11-13 20:44:04'),('1592159323008655362','<p>抓电子小鬼</p>','2022-11-14 22:15:38','2022-11-28 12:28:25'),('1592530559270125570','<p>十分大方</p>','2022-11-15 22:50:48','2022-11-15 22:50:48'),('1593471447802585089','<p>e发送</p>','2022-11-18 13:09:33','2022-11-18 13:09:33'),('1593586704738996226','<p>测试</p>','2022-11-18 20:47:32','2022-11-18 20:47:32'),('1593598715006926849','<p>烦烦烦</p>','2022-11-18 21:35:16','2022-11-18 21:35:16'),('1593599365505728514','<p>阿萨大大</p>','2022-11-18 21:37:51','2022-11-18 21:37:51'),('1593602718482579457','<p>六六六</p>','2022-11-18 21:51:10','2022-11-18 21:51:10'),('1594320414681595906','<p>12345678</p>','2022-11-20 21:23:03','2022-11-26 16:27:02'),('1594320728109350914','<p>并加快改革i经济</p>','2022-11-20 21:24:17','2022-11-21 11:10:50'),('1594321225629302785','<p>阿萨的绝大部分</p>','2022-11-20 21:26:16','2022-11-26 16:47:15'),('1594321309020454914','<p>阿凡达</p>','2022-11-20 21:26:36','2022-11-20 21:26:36'),('1594322011138555906','<p>论如何把王友踩在脚底，让他喊爸爸</p>','2022-11-20 21:29:23','2022-11-26 13:39:39'),('1594322558847549441','<p>速度放缓的法国咋</p>','2022-11-20 21:31:34','2022-11-26 16:47:43'),('1596480241566482434','<p>撒范德萨</p>','2022-11-26 20:25:25','2022-11-26 20:25:25'),('1605906254419505153','','2022-12-22 20:41:02','2022-12-22 20:41:02'),('18','<p>本套Java视频完全针对零基础学员，课堂实录，自发布以来，好评如潮！Java视频中注重与学生互动，讲授幽默诙谐、细致入微，覆盖Java基础所有核心知识点，同类Java视频中也是代码量大、案例多、实战性强的。同时，本Java视频教程注重技术原理剖析，深入JDK源码，辅以代码实战贯穿始终，用实践驱动理论，并辅以必要的代码练习。</p>\n<p>------------------------------------</p>\n<p><strong>视频特点：</strong></p>\n<p>通过学习本Java视频教程，大家能够真正将Java基础知识学以致用、活学活用，构架Java编程思想，牢牢掌握\"源码级\"的Javase核心技术，并为后续JavaWeb等技术的学习奠定扎实基础。<br /><br />1.通俗易懂，细致入微：每个知识点高屋建瓴，深入浅出，简洁明了的说明问题<br />2.具实战性：全程真正代码实战，涵盖上百个企业应用案例及练习<br />3.深入：源码分析，更有 Java 反射、动态代理的实际应用等<br />4.登录尚硅谷官网，技术讲师免费在线答疑</p>','2019-03-06 18:06:36','2022-11-27 13:14:38');

/*Table structure for table `edu_subject` */

DROP TABLE IF EXISTS `edu_subject`;

CREATE TABLE `edu_subject` (
  `id` char(19) NOT NULL COMMENT '课程类别ID',
  `title` varchar(10) NOT NULL COMMENT '类别名称',
  `parent_id` char(19) NOT NULL DEFAULT '0' COMMENT '父ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序字段',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程科目';

/*Data for the table `edu_subject` */

insert  into `edu_subject`(`id`,`title`,`parent_id`,`sort`,`gmt_create`,`gmt_modified`) values ('1589896160762433538','后端开发','0',0,'2022-11-08 16:22:38','2022-11-08 16:22:38'),('1589896160829542401','Java','1589896160762433538',0,'2022-11-08 16:22:38','2022-11-08 16:22:38'),('1589896160896651265','前端开发','0',0,'2022-11-08 16:22:38','2022-11-08 16:22:38'),('1589896160959565825','Vue','1589896160896651265',0,'2022-11-08 16:22:38','2022-11-08 16:22:38'),('1589896161026674689','C++','1589896160762433538',0,'2022-11-08 16:22:38','2022-11-08 16:22:38'),('1590174828738940930','JavaScript','1589896160896651265',0,'2022-11-09 10:49:58','2022-11-09 10:49:58'),('1590174828806049793','PHP','1589896160762433538',0,'2022-11-09 10:49:58','2022-11-09 10:49:58'),('1590922444481282050','数据库开发','0',0,'2022-11-11 12:20:43','2022-11-11 12:20:43'),('1590922444481282051','MySQL','1590922444481282050',0,'2022-11-11 12:20:43','2022-11-11 12:20:43');

/*Table structure for table `edu_teacher` */

DROP TABLE IF EXISTS `edu_teacher`;

CREATE TABLE `edu_teacher` (
  `id` char(19) NOT NULL COMMENT '讲师ID',
  `name` varchar(20) NOT NULL COMMENT '讲师姓名',
  `intro` varchar(500) NOT NULL DEFAULT '' COMMENT '讲师简介',
  `career` varchar(500) DEFAULT NULL COMMENT '讲师资历,一句话说明讲师',
  `level` int(10) unsigned NOT NULL COMMENT '头衔 1高级讲师 2首席讲师',
  `avatar` varchar(255) DEFAULT NULL COMMENT '讲师头像',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='讲师';

/*Data for the table `edu_teacher` */

insert  into `edu_teacher`(`id`,`name`,`intro`,`career`,`level`,`avatar`,`sort`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1','丽丽','string','string',0,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/b4443bf71a7842dbb150c0f3c01078da.png',0,0,'2019-10-30 14:18:46','2022-11-01 21:37:03'),('1189389726308478977','晴天','高级讲师简介','高级讲师资历',2,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/b4443bf71a7842dbb150c0f3c01078da.png',1,0,'2019-10-30 11:53:03','2019-10-30 11:53:03'),('1189390295668469762','李刚','高级讲师简介','高级讲师',2,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/b4443bf71a7842dbb150c0f3c01078da.png',2,0,'2019-10-30 11:55:19','2019-11-12 13:37:52'),('1189426437876985857','王二','高级讲师简介','高级讲师',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/b4443bf71a7842dbb150c0f3c01078da.png',0,0,'2019-10-30 14:18:56','2019-11-12 13:37:35'),('1189426464967995393','王五','高级讲师简介','高级讲师',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/9f95eaf2fd47433d98228ff417983eb7.png',0,0,'2019-10-30 14:19:02','2022-11-21 11:18:00'),('1192249914833055746','李四','高级讲师简介','高级讲师',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/a15e0fecc60d408e8447ecff095f5968.png',0,0,'2019-11-07 09:18:25','2022-11-21 11:17:50'),('1587433401059639298','程','1010','string',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/b4443bf71a7842dbb150c0f3c01078da.png',0,0,'2022-11-01 21:16:31','2022-11-21 11:17:32'),('1589179206145757185','王琪','非常6666','66666',1,NULL,1,1,'2022-11-06 16:53:43','2022-11-06 23:03:07'),('1589272230573944834','程克','66666','666',2,NULL,0,1,'2022-11-06 23:03:22','2022-11-06 23:03:22'),('1589814014542655490','威威','22222','1111',2,'',1,1,'2022-11-08 10:56:13','2022-11-08 10:56:13'),('1592534305597325313','王大琪666','上过学，学的不咋地','非常6',1,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/15/28fb19b1926a4bc486e4b6ea1501f557.png',1,0,'2022-11-15 23:05:41','2022-11-27 15:08:45'),('1596381214795735041','发多少个','士大夫','是否',2,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/26/bf190e6d21574636b1a12558a36a851c.png',0,0,'2022-11-26 13:51:56','2022-11-26 14:23:23'),('1596381268566712321','第三方','艾弗森','萨芬地方',2,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/26/00871aaf19ed4f0a9fe4175bdc8cdd48.png',0,0,'2022-11-26 13:52:08','2022-11-26 14:23:01'),('1596381312531406850','vdaa是v现在',' 士大夫啊方法','现在v',2,'https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/26/9b09ad0f8a9040ada714224987b2fffd.png',0,0,'2022-11-26 13:52:19','2022-11-26 13:54:29');

/*Table structure for table `edu_video` */

DROP TABLE IF EXISTS `edu_video`;

CREATE TABLE `edu_video` (
  `id` char(19) NOT NULL COMMENT '视频ID',
  `course_id` char(19) NOT NULL COMMENT '课程ID',
  `chapter_id` char(19) NOT NULL COMMENT '章节ID',
  `title` varchar(50) NOT NULL COMMENT '节点名称',
  `video_source_id` varchar(100) DEFAULT NULL COMMENT '云端视频资源',
  `video_original_name` varchar(100) DEFAULT NULL COMMENT '原始文件名称',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序字段',
  `play_count` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '播放次数',
  `is_free` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否可以试听：0收费 1免费',
  `duration` float NOT NULL DEFAULT '0' COMMENT '视频时长（秒）',
  `status` varchar(20) NOT NULL DEFAULT 'Empty' COMMENT 'Empty未上传 Transcoding转码中  Normal正常',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '视频源文件大小（字节）',
  `version` bigint(20) unsigned NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_chapter_id` (`chapter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程视频';

/*Data for the table `edu_video` */

insert  into `edu_video`(`id`,`course_id`,`chapter_id`,`title`,`video_source_id`,`video_original_name`,`sort`,`play_count`,`is_free`,`duration`,`status`,`size`,`version`,`gmt_create`,`gmt_modified`) values ('1592159469444390913','1592159323008655362','1592159418819141634','电子小鬼现身术烦烦烦','','',0,0,0,0,'Empty',0,1,'2022-11-14 22:16:13','2022-11-14 22:28:49'),('1593826946158972929','1592159323008655362','1592159418819141634','222','6760d45acba8478aa995e0d80eb7da55','6 - What If I Want to Move Faster.mp4',0,0,0,0,'Empty',0,1,'2022-11-19 12:42:11','2022-11-19 12:42:11'),('1596721256768585730','18','1593129529566568449','第二节 666','6760d45acba8478aa995e0d80eb7da55','',0,0,0,0,'Empty',0,1,'2022-11-27 12:23:08','2022-11-27 12:23:08'),('1596732021340749826','18','1593129529566568449','222','6760d45acba8478aa995e0d80eb7da55','6 - What If I Want to Move Faster.mp4',0,0,0,0,'Empty',0,1,'2022-11-27 13:05:54','2022-11-27 13:05:54');

/*Table structure for table `member` */

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `member` */

insert  into `member`(`id`,`username`,`age`) values (1,'che',20),(33,'qi',19);

/*Table structure for table `statistics_daily` */

DROP TABLE IF EXISTS `statistics_daily`;

CREATE TABLE `statistics_daily` (
  `id` char(19) NOT NULL COMMENT '主键',
  `date_calculated` varchar(20) NOT NULL COMMENT '统计日期',
  `register_num` int(11) NOT NULL DEFAULT '0' COMMENT '注册人数',
  `login_num` int(11) NOT NULL DEFAULT '0' COMMENT '登录人数',
  `video_view_num` int(11) NOT NULL DEFAULT '0' COMMENT '每日播放视频数',
  `course_num` int(11) NOT NULL DEFAULT '0' COMMENT '每日新增课程数',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `statistics_day` (`date_calculated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网站统计日数据';

/*Data for the table `statistics_daily` */

insert  into `statistics_daily`(`id`,`date_calculated`,`register_num`,`login_num`,`video_view_num`,`course_num`,`gmt_create`,`gmt_modified`) values ('1599683601136365569','2019-01-19',6,184,161,143,'2022-12-05 16:34:26','2022-12-05 16:34:26'),('1599728491463454722','2022-12-01',55,156,163,137,'2022-12-05 19:32:49','2022-12-05 19:32:49'),('1599939655279583234','2022-12-05',65,152,162,117,'2022-12-06 09:31:54','2022-12-06 09:31:54'),('1600325278020792321','2022-12-06',0,136,138,168,'2022-12-07 11:04:14','2022-12-07 11:04:14'),('1601545920086929409','2022-12-09',0,163,177,163,'2022-12-10 19:54:37','2022-12-10 19:54:37');

/*Table structure for table `t_order` */

DROP TABLE IF EXISTS `t_order`;

CREATE TABLE `t_order` (
  `id` char(19) NOT NULL DEFAULT '',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `course_id` varchar(19) NOT NULL DEFAULT '' COMMENT '课程id',
  `course_title` varchar(100) DEFAULT NULL COMMENT '课程名称',
  `course_cover` varchar(255) DEFAULT NULL COMMENT '课程封面',
  `teacher_name` varchar(20) DEFAULT NULL COMMENT '讲师名称',
  `member_id` varchar(19) NOT NULL DEFAULT '' COMMENT '会员id',
  `nickname` varchar(50) DEFAULT NULL COMMENT '会员昵称',
  `mobile` varchar(11) DEFAULT NULL COMMENT '会员手机',
  `total_fee` decimal(10,2) DEFAULT '0.01' COMMENT '订单金额（分）',
  `pay_type` tinyint(3) DEFAULT NULL COMMENT '支付类型（1：微信 2：支付宝）',
  `status` tinyint(3) DEFAULT NULL COMMENT '订单状态（0：未支付 1：已支付）',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_order_no` (`order_no`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单';

/*Data for the table `t_order` */

insert  into `t_order`(`id`,`order_no`,`course_id`,`course_title`,`course_cover`,`teacher_name`,`member_id`,`nickname`,`mobile`,`total_fee`,`pay_type`,`status`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1598955615995076610','20221203162140858','18','18无敌','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/8d8207a076e84c239fc11d57b93fdf7c.jpg','程','1195187659054329857','che','17364450905','0.01',1,1,0,'2022-12-03 16:21:41','2022-12-03 16:22:49'),('1599379301428404226','20221204202515563','18','18无敌','https://edu-chengke.oss-cn-nanjing.aliyuncs.com/2022/11/21/8d8207a076e84c239fc11d57b93fdf7c.jpg','程','1594999162565296129','che','17364450905','0.01',1,1,0,'2022-12-04 20:25:15','2022-12-04 20:25:41');

/*Table structure for table `t_pay_log` */

DROP TABLE IF EXISTS `t_pay_log`;

CREATE TABLE `t_pay_log` (
  `id` char(19) NOT NULL DEFAULT '',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `pay_time` datetime DEFAULT NULL COMMENT '支付完成时间',
  `total_fee` decimal(10,2) DEFAULT '0.01' COMMENT '支付金额（分）',
  `transaction_id` varchar(30) DEFAULT NULL COMMENT '交易流水号',
  `trade_state` char(20) DEFAULT NULL COMMENT '交易状态',
  `pay_type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '支付类型（1：微信 2：支付宝）',
  `attr` text COMMENT '其他属性',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_order_no` (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='支付日志表';

/*Data for the table `t_pay_log` */

insert  into `t_pay_log`(`id`,`order_no`,`pay_time`,`total_fee`,`transaction_id`,`trade_state`,`pay_type`,`attr`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1598955903363620865','20221203162140858','2022-12-03 16:22:49','0.01','4200001665202212030844218519','SUCCESS',1,'{\"transaction_id\":\"4200001665202212030844218519\",\"nonce_str\":\"ZBc8vtM7lMe25DVa\",\"trade_state\":\"SUCCESS\",\"bank_type\":\"OTHERS\",\"openid\":\"oHwsHuKzsuU8gP-UgFgbzbwgW9nA\",\"sign\":\"84C2F4144CB123B2DC1B360F4B75BB1B\",\"return_msg\":\"OK\",\"fee_type\":\"CNY\",\"mch_id\":\"1558950191\",\"cash_fee\":\"1\",\"out_trade_no\":\"20221203162140858\",\"cash_fee_type\":\"CNY\",\"appid\":\"wx74862e0dfcf69954\",\"total_fee\":\"1\",\"trade_state_desc\":\"支付成功\",\"trade_type\":\"NATIVE\",\"result_code\":\"SUCCESS\",\"attach\":\"\",\"time_end\":\"20221203162246\",\"is_subscribe\":\"N\",\"return_code\":\"SUCCESS\"}',0,'2022-12-03 16:22:49','2022-12-03 16:22:49'),('1599379410480308225','20221204202515563','2022-12-04 20:25:41','0.01','4200001673202212042656703075','SUCCESS',1,'{\"transaction_id\":\"4200001673202212042656703075\",\"nonce_str\":\"MVwLVyTpo11ew32B\",\"trade_state\":\"SUCCESS\",\"bank_type\":\"OTHERS\",\"openid\":\"oHwsHuKzsuU8gP-UgFgbzbwgW9nA\",\"sign\":\"2AE6DCD4102C4EB206D5B369D5C9168A\",\"return_msg\":\"OK\",\"fee_type\":\"CNY\",\"mch_id\":\"1558950191\",\"cash_fee\":\"1\",\"out_trade_no\":\"20221204202515563\",\"cash_fee_type\":\"CNY\",\"appid\":\"wx74862e0dfcf69954\",\"total_fee\":\"1\",\"trade_state_desc\":\"支付成功\",\"trade_type\":\"NATIVE\",\"result_code\":\"SUCCESS\",\"attach\":\"\",\"time_end\":\"20221204202537\",\"is_subscribe\":\"N\",\"return_code\":\"SUCCESS\"}',0,'2022-12-04 20:25:41','2022-12-04 20:25:41');

/*Table structure for table `ucenter_member` */

DROP TABLE IF EXISTS `ucenter_member`;

CREATE TABLE `ucenter_member` (
  `id` char(19) NOT NULL COMMENT '会员id',
  `openid` varchar(128) DEFAULT NULL COMMENT '微信openid',
  `mobile` varchar(11) DEFAULT '' COMMENT '手机号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `sex` tinyint(2) unsigned DEFAULT NULL COMMENT '性别 1 女，2 男',
  `age` tinyint(3) unsigned DEFAULT NULL COMMENT '年龄',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `sign` varchar(100) DEFAULT NULL COMMENT '用户签名',
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否禁用 1（true）已禁用，  0（false）未禁用',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会员表';

/*Data for the table `ucenter_member` */

insert  into `ucenter_member`(`id`,`openid`,`mobile`,`password`,`nickname`,`sex`,`age`,`avatar`,`sign`,`is_disabled`,`is_deleted`,`gmt_create`,`gmt_modified`) values ('1',NULL,'13700000001','96e79218965eb72c92a549dd5a330112','小三123',1,5,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-01-01 12:11:33','2019-11-08 11:56:01'),('1080736474267144193',NULL,'13700000011','96e79218965eb72c92a549dd5a330112','用户XJtDfaYeKk',1,19,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-01-02 12:12:45','2019-01-02 12:12:56'),('1080736474355224577',NULL,'13700000002','96e79218965eb72c92a549dd5a330112','用户wUrNkzAPrc',1,27,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-01-02 12:13:56','2019-01-02 12:14:07'),('1086387099449442306',NULL,'13520191388','96e79218965eb72c92a549dd5a330112','用户XTMUeHDAoj',2,20,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-01-19 06:17:23','2019-01-19 06:17:23'),('1086387099520745473',NULL,'13520191389','96e79218965eb72c92a549dd5a330112','用户vSdKeDlimn',1,21,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-01-19 06:17:23','2019-01-19 06:17:23'),('1086387099608825858',NULL,'13520191381','96e79218965eb72c92a549dd5a330112','用户EoyWUVXQoP',1,18,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-01-19 06:17:23','2019-01-19 06:17:23'),('1086387099701100545',NULL,'13520191382','96e79218965eb72c92a549dd5a330112','用户LcAYbxLNdN',2,24,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-01-19 06:17:23','2019-01-19 06:17:23'),('1086387099776598018',NULL,'13520191383','96e79218965eb72c92a549dd5a330112','用户dZdjcgltnk',2,25,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-01-19 06:17:23','2019-01-19 06:17:23'),('1086387099852095490',NULL,'13520191384','96e79218965eb72c92a549dd5a330112','用户wNHGHlxUwX',2,23,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-01-19 06:17:23','2019-01-19 06:17:23'),('1106746895272849410','o1R-t5u2TfEVeVjO9CPGdHPNw-to',NULL,NULL,'檀梵\'',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/zZfLXcetf2Rpsibq6HbPUWKgWSJHtha9y1XBeaqluPUs6BYicW1FJaVqj7U3ozHd3iaodGKJOvY2PvqYTuCKwpyfQ/132',NULL,0,0,'2019-03-16 10:39:57','2019-03-16 10:39:57'),('1106822699956654081',NULL,NULL,NULL,NULL,NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-03-16 15:41:10','2019-03-16 15:41:10'),('1106823035660357634','o1R-t5i4gENwHYRb5lVFy98Z0bdk',NULL,NULL,'GaoSir',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJI53RcCuc1no02os6ZrattWGiazlPnicoZQ59zkS7phNdLEWUPDk8fzoxibAnXV1Sbx0trqXEsGhXPw/132',NULL,0,0,'2019-03-16 15:42:30','2019-03-16 15:42:30'),('1106823041599492098',NULL,NULL,NULL,NULL,NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-03-16 15:42:32','2019-03-16 15:42:32'),('1106823115788341250','o1R-t5l_3rnbZbn4jWwFdy6Gk6cg',NULL,NULL,'换个网名哇、',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/jJHyeM0EN2jhB70LntI3k8fEKe7W6CwykrKMgDJM4VZqCpcxibVibX397p0vmbKURGkLS4jxjGB0GpZfxCicgt07w/132',NULL,0,0,'2019-03-16 15:42:49','2019-03-16 15:42:49'),('1106826046730227714','o1R-t5gyxumyBqt0CWcnh0S6Ya1g',NULL,NULL,'我是Helen',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKDRfib8wy7A2ltERKh4VygxdjVC1x5OaOb1t9hot4JNt5agwaVLdJLcD9vJCNcxkvQnlvLYIPfrZw/132',NULL,0,0,'2019-03-16 15:54:28','2019-03-16 15:54:28'),('1106828185829490690','o1R-t5nNlou5lRwBVgGNJFm4rbc4',NULL,NULL,' 虎头',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKxCqRzuYWQmpwiaqQEjNxbC7WicebicXQusU306jgmfoOzUcFg1qaDq5BStiblwBjw5dUOblQ2gUicQOQ/132',NULL,0,0,'2019-03-16 16:02:58','2019-03-16 16:02:58'),('1106830599651442689','o1R-t5hZHQB1cbX7HZJsiM727_SA',NULL,NULL,'是吴啊',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ9CsqApybcs7f3Dyib9IxIh0sBqJb7LicbjU4WticJFF0PVwFvHgtbFdBwfmk3H2t3NyqmEmVx17tRA/132',NULL,0,0,'2019-03-16 16:12:34','2019-03-16 16:12:34'),('1106830976199278593','o1R-t5meKOoyEJ3-IhWRCBKFcvzU',NULL,NULL,'我才是Helen',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epMicP9UT6mVjYWdno0OJZkOXiajG0sllJTbGJ9DYiceej2XvbDSGCK8LCF7jv1PuG2uoYlePWic9XO8A/132',NULL,0,0,'2019-03-16 16:14:03','2019-03-16 16:14:03'),('1106831936900415490','o1R-t5jXYSWakGtnUBnKbfVT5Iok',NULL,NULL,'文若姬',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/3HEmJwpSzguqqAyzmBwqT6aicIanswZibEOicQInQJI3ZY1qmu59icJC6N7SahKqWYv24GvX5KH2fibwt0mPWcTJ3fg/132',NULL,0,0,'2019-03-16 16:17:52','2019-03-16 16:17:52'),('1106832491064442882','o1R-t5sud081Qsa2Vb2xSKgGnf_g',NULL,NULL,'Peanut',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-03-16 16:20:04','2019-03-16 16:20:04'),('1106833021442510849','o1R-t5lsGc3I8P5bDpHj7m_AIRvQ',NULL,NULL,'食物链终结者',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/MQ7qUmCprK9am16M1Ia1Cs3RK0qiarRrl9y8gsssBjIZeS2GwKSrnq7ZYhmrzuzDwBxSMMAofrXeLic9IBlW4M3Q/132',NULL,0,0,'2019-03-16 16:22:11','2019-03-16 16:22:11'),('1191600824445046786',NULL,'15210078344','96e79218965eb72c92a549dd5a330112','IT妖姬',1,5,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-11-05 14:19:10','2019-11-08 18:04:43'),('1191616288114163713',NULL,'17866603606','96e79218965eb72c92a549dd5a330112','xiaowu',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-11-05 15:20:37','2019-11-05 15:20:37'),('1195187659054329857',NULL,'17364450906','96e79218965eb72c92a549dd5a330112','qy',NULL,NULL,'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132',NULL,0,0,'2019-11-15 11:51:58','2019-11-15 11:51:58'),('1594999162565296129',NULL,'17364450905','96e79218965eb72c92a549dd5a330112','che',NULL,NULL,'http://aliyun_id_photo_bucket.oss.aliyuncs.com/default_handsome.jpg',NULL,0,0,'2022-11-22 18:20:09','2022-11-22 18:20:09'),('1595702079748308994','o3_SC51J6WQGtJ1Z6uFy0Qn6K2kM','',NULL,'GO',NULL,NULL,'https://thirdwx.qlogo.cn/mmopen/vi_32/bGm9tDfNT4CibhbgynsVKSN37ecIKSwmtXlmO0TFngjyicNWljict4lfMuLiatialEVgMOFRlsQKibddlwvZQSYqp3Qg/132',NULL,0,0,'2022-11-24 16:53:17','2022-11-24 16:53:17'),('1597084291500728322','o3_SC52M9Cgvg0Tr7P3lh-iN743A','',NULL,'灰太狼的温柔',NULL,NULL,'https://thirdwx.qlogo.cn/mmopen/vi_32/QiahHkFZqzAQb2gVh963VviaeJ3WGSroZpq75r7pKsfvLtf5kgQbs0sOdENaVQy8uuUz49YbK6diaAJ8UcPKyCTvg/132',NULL,0,0,'2022-11-28 12:25:42','2022-11-28 12:25:42'),('1597499492427403266','o3_SC52bOgNBBtZ7IYBkRQ0FEyS4','',NULL,'~',NULL,NULL,'https://thirdwx.qlogo.cn/mmopen/vi_32/DiasSKoTz6nPFWHrsClsnBjklibLCdnLlNnMoLclLDzjVo3DwzQDpg4iba98ZJib1wwFuMNMwUSiaOwFAHuv9SvngBw/132',NULL,0,0,'2022-11-29 15:55:34','2022-11-29 15:55:34');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
