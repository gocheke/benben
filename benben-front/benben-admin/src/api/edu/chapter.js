import request from '@/utils/request'

export default {

    getAllChapterVideo(courseId){
        return request({
            url: `/eduservice/chapter/getChapterVideo/${courseId}`,
            method: 'get'
        })
    },
    saveChapter(chapter){
        return request({
            url: `/eduservice/chapter/addChapter`,
            method: 'post',
            data: chapter
        })
    },
    getChapterInfoById(chapterId){
        return request({
            url: `/eduservice/chapter/getChapterInfo/${chapterId}`,
            method: 'get'
        })
    },
    updateChapterInfo(chapter){
        return request({
            url: `/eduservice/chapter/updateChapterInfo`,
            method: 'post',
            data: chapter
        })
    },
    deleteChapterById(chapterId){
        return request({
            url: `/eduservice/chapter/deleteChapter/${chapterId}`,
            method: 'delete'
        })
    },
    //=========================小节================================
    addVideoInfo(video){
        return request({
            url: `/eduservice/video/addVideo`,
            method: 'post',
            data: video
        })
    },
    updateVideoInfo(video){
        return request({
            url: `/eduservice/video/updateVideo/${video.id}`,
            method: 'post',
            data:video
        })
    },
    deleteVideoInfoById(videoId){
        return request({
            url: `/eduservice/video/deleteVideo/${videoId}`,
            method: 'delete'
        })
    },
    getVideoInfoById(id){
        return request({
            url: `/eduservice/video/getVideoInfo/${id}`,
            method: 'get'
        })
    }
}
//                                                          _ooOoo_
//                                                         o8888888o
//                                                         88" . "88
//                                                         (| -_- |)
//                                                          O\ = /O
//                                                      ____/`---'\____
//                                                    .   ' \\| |// `.
//                                                     / \\||| : |||// \
//                                                   / _||||| -:- |||||- \
//                                                     | | \\\ - /// | |
//                                                   | \_| ''\---/'' | |
//                                                    \ .-\__ `-` ___/-. /
//                                                 ___`. .' /--.--\ `. . __
//                                              ."" '< `.___\_<|>_/___.' >'"".
//                                             | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                                               \ \ `-. \_ __\ /__ _/ .-` / /
//                                       ======`-.____`-.___\_____/___.-`____.-'======
//                                                          `=---='
//
//                                       .............................................
//                                              佛祖保佑             永无BUG