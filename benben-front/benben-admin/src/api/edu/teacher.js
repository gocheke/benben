import request from '@/utils/request'

export default {

    // 1.讲师列表，条件查询分页
    // current当前页 limit每页记录数 teacherQuery条件对象
    getTeacherListPage(current,limit,teacherQuery){
        return request({
            url: `/eduservice/teacher/pageTeacherCondition/${current}/${limit}`,
            method: 'post',
            // teacherQuery条件对象，后端采用RequestBody注解取得
            // data表示将对象转换成json数据传进接口
            data: teacherQuery
        })
    },
    deleteTeacherById(id){
        return request({
            url: `/eduservice/teacher/${id}`,
            method: 'delete',
            // teacherQuery条件对象，后端采用RequestBody注解取得
            // data表示将对象转换成json数据传进接口
        })
    },
    addTeacher(teacher){
        return request({
            url: `/eduservice/teacher/addTeacher`,
            method: 'post',
            data: teacher
        })
    },
    updateTeacherApi(teacher){
        return request({
            url: `/eduservice/teacher/updateTeacher`,
            method: 'post',
            data: teacher
        })
    },
    getTeacherInfo(id){
        return request({
            url: `/eduservice/teacher/getTeacher/${id}`,
            method: 'get'
        })
    }
}

