import request from '@/utils/request'

export default {

    removeVideoById(id){
        return request({
            url: `/eduservice/video/deleteVideo/${id}`,
            method: 'delete'
        })
    },
    removeAliyunVideoBySourceVideoId(sourceVideoId){
        return request({
            url: `/eduvod/video/deleteVideo/${id}`,
            method: 'delete'
        })
    },

}