import request from '@/utils/request'

export default {

    getAllChapterVideo(page,limit){
        return request({
            url: `/educms/banneradmin/${page}/${limit}`,
            method: 'get'
        })
    },
    removeBannerById(id){
        return request({
            url: `/educms/banneradmin/remove/${id}`,
            method: 'delete'
        })
    },
    addBanner(banner){
        return request({
            url: `/educms/banneradmin/save`,
            method: 'post',
            data:banner
        })
    },
    updateBanner(banner){
        return request({
            url: `/educms/banneradmin/update`,
            method: 'put',
            data:banner
        })
    },
    getBannerById(id){
        return request({
            url: `/educms/banneradmin/get/${id}`,
            method: 'get',
        })
    }

}