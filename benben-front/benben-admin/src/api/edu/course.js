import request from '@/utils/request'

export default {

    addCourseInfo(courseInfo){
        return request({
            url: `/eduservice/course/addCourseInfo`,
            method: 'post',
            data:courseInfo
        })
    },
    getTeacherList(){
        return request({
            url: `/eduservice/teacher/findAll`,
            method: 'get'
        })
    },
    getSubjectParentList(){
        return request({
            url: `/eduservice/subject/getAllSubject`,
            method: 'get'
        })
    },
    getCourseInfoById(courseId){
        return request({
            url: `/eduservice/course/getCourseInfo/${courseId}`,
            method: 'get'
        })
    },
    updateCourseInfo(courseInfo){
        return request({
            url: `/eduservice/course/updateCourseInfo`,
            method: 'post',
            data:courseInfo
        })
    },
    getAllCourselist(){
        return request({
            url: `/eduservice/course/getAllCourseList`,
            method: 'get',
        })
    },
    getCourseListPage(current,limit,courseQuery){
        return request({
            url: `/eduservice/course/getCourseListByCourseQuery/${current}/${limit}`,
            method: 'post',
            data:courseQuery
        })
    },
    deleteCourseById(courseId){
        return request({
            url: `/eduservice/course/deleteCourse/${courseId}`,
            method: 'delete',
        })
    }
}

