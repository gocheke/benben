import request from '@/utils/request'

export default {

    getPublishInfoByCourseId(courseId){
        return request({
            url: `/eduservice/course/getAllPublish/${courseId}`,
            method: 'get'
        })
    },
    publishCourseByCourseId(courseId){
        return request({
            url: `/eduservice/course/publishCourse/${courseId}`,
            method: 'post'
        })
    }

}