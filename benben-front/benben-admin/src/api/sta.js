import request from '@/utils/request'

export default {

    createStaData(day){
        return request({
            url: `/stasevice/sta/registerCount/${day}`,
            method: 'get'
        })
    },
    getDataSta(searchObj){
        return request({
            url: `/stasevice/sta/showData/${searchObj.type}/${searchObj.begin}/${searchObj.end}`,
            method: 'get'
        })
    }

}