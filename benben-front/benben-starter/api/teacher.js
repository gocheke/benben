import request from "@/utils/request"

export default {
  getTeacherList(page,limit) {
    return request({
      url: `eduservice/teacherfront/getTeacherFrontList/${page}/${limit}`,
      method: 'get'
    })
  },
  getTeacherById(id) {
    return request({
      url: `/eduservice/teacher/getTeacherInfo/${id}`,
      method: 'get'
    })
  },
}