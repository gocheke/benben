import request from "@/utils/request"

export default {
  sendCodeByPhone(phone) {
    return request({
      url: `/edumsm/msm/send/${phone}`,
      method: 'get'
    })
  },
  register(fromItem) {
    return request({
      url: `/educenter/member/register`,
      method: 'post',
      data: fromItem
    })
  }
}