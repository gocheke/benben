import request from "@/utils/request"

export default {
  getAllCourseAndTeacher() {
    return request({
      url: `/eduservice/indexfront/index`,
      method: 'get'
    })
  },
  getPageList(page, limit, searchObj) {
    return request({
      url: `/eduservice/coursefront/getFrontCourseList/${page}/${limit}`,
      method: 'post',
      data: searchObj
    })
  },
  // 获取课程二级分类
  getNestedTreeList2() {
    return request({
      url: `/eduservice/subject/getAllSubject`,
      method: 'get'
    })
  },
  getCourseWebInfoByCourseId(id) {
    return request({
      url: `/eduservice/course/getCourseWebInfo/${id}`,
      method: 'get'
    })
  }
}