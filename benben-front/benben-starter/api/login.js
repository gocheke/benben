import request from "@/utils/request"

export default {
    submitLoginUser(loginInfo) {
    return request({
      url: `/educenter/member/login`,
      method: 'post',
      data: loginInfo
    })
  },
  getLoginUserInfo() {
    return request({
      url: `/educenter/member/auth/getLoginInfo`,
      method: 'get'
    })
  },
}